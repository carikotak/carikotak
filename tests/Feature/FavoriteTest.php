<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Product;
use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FavoriteTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeFavoritePage()
    {
        $user       = User::first();
        $this->actingAs($user, 'web');

        $response = $this->get('wishlist');
        $response->assertStatus(200);
    }

    public function testUserCanGetFavoriteProduct()
    {
        $user       = User::first();
        $this->actingAs($user, 'api');

        $response = $this->get('api/me/favorite-products');
        $response->assertStatus(200);
    }

    public function testUserCanGetFavoritePost()
    {
        $user       = User::first();
        $this->actingAs($user, 'api');

        $response = $this->get('api/me/favorite-posts');
        $response->assertStatus(200);
    }

    public function testUserCanFavoriteProduct()
    {
        $this->seed('CategoriesTableSeeder');
        $this->seed('MaterialSeeder');
        $this->seed('ProductsTableSeeder');

        $user       = User::first();
        $product    = Product::first();

        $this->actingAs($user, 'api');
        
        $response   = $this->put('api/favorite/' . $product->id . '?type=product');

        $response->assertStatus(200);
    }

    public function testUserCanUnFavoriteProduct()
    {
        $this->seed('CategoriesTableSeeder');
        $this->seed('MaterialSeeder');
        $this->seed('ProductsTableSeeder');

        $user       = User::first();
        $product    = Product::first();

        $this->actingAs($user, 'api');
        
        $response   = $this->delete('api/favorite/' . $product->id . '?type=product');

        $response->assertStatus(200);
    }

    public function testUserCanFavoritePost()
    {
        $this->seed('PostsTableSeeder');

        $user       = User::first();
        $post       = Post::first();

        $this->actingAs($user, 'api');
        
        $response   = $this->put('api/favorite/' . $post->id . '?type=post');

        $response->assertStatus(200);
    }

    public function testUserCanUnFavoritePost()
    {
        $this->seed('PostsTableSeeder');

        $user       = User::first();
        $post       = Post::first();

        $this->actingAs($user, 'api');
        
        $response   = $this->delete('api/favorite/' . $post->id . '?type=post');

        $response->assertStatus(200);
    }
}
