<?php

namespace Tests\Feature\Guest;

use Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    
    public function testGuestCanSeeLoginPage()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function testGuestLoginNull()
    {
        $data = [
            'email'     => null,
            'password'  => null
        ];
        $response = $this->json('POST', 'login', $data);
        $response->assertStatus(400);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testGuestLoginSomeNullField()
    {
        $data = [
            'email'     => null,
            'password'  => 'qwe123123'
        ];

        $response = $this->json('POST', 'login', $data);
        $response->assertStatus(400);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testGuestLoginWithIncorrectCredential()
    {
        $data = [
            'email'     => 'riskteria@gmail.com',
            'password'  => 'qwe123123x'
        ];

        $response = $this->json('POST', 'login', $data);
        $response->assertStatus(401);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testUserLoginWithCorrectCredential()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');

        $data = [
            'email'     => 'admin@admin.com',
            'password'  => 'password',
        ];

        $response = $this->json('POST', 'login', $data);
        $response->assertStatus(200);
        $response->json();
        $this->assertTrue(Auth::check());
    }
}
