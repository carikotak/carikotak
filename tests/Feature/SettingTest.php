<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SettingTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeSettingPage()
    {
        $user = User::first();

        $this->actingAs($user, 'web');
        
        $response = $this->get('settings');
        $response->assertStatus(200);

    }

    public function testUserCanSeeAccountSettingPage()
    {
        $user = User::first();

        $this->actingAs($user, 'web');
        
        $response = $this->get('settings/account');
        $response->assertStatus(200);
    }
}
