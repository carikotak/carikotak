<?php

namespace Tests\Feature;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StoryPageTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setUp()
    {
        parent::setUp();
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');
        $this->seed('PostsTableSeeder');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeStoryPage()
    {
        $post = Post::first();

        $response = $this->get('post/' . $post->slug);

        $response->assertStatus(200);
    }
}
