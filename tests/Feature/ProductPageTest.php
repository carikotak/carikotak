<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\Product;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductPageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeProductPage()
    {
        $this->seed('MaterialSeeder');
        $this->seed('ProductsTableSeeder');
        
        $product = Product::first();
        
        $response = $this->get('product/' . $product->slug);
        $response->assertStatus(200);
    }
}
