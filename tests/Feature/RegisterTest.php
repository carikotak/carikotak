<?php

namespace Tests\Feature;

use Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testGuestCanSeeRegisterPage()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
    }

    public function testGuestRegisterNull()
    {
        $data = [
            'email'     => null,
            'password'  => null,
            'username'  => null,
            'name'      => null
        ];
        $response = $this->json('POST', 'register', $data);
        $response->assertStatus(422);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testGuestRegisterSomeNullField()
    {
        $data = [
            'email'     => null,
            'password'  => 'qwe123123',
            'username'  => 'fsfdsfds',
            'name'      => null
        ];

        $response = $this->json('POST', 'register', $data);
        $response->assertStatus(422);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testGuestRegisterWithExistingEmail()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');

        $data = [
            'email'     => 'admin@admin.com',
            'password'  => 'qwe123123x',
            'name'      => 'admin',
            'username'  => 'riskteria'
        ];

        $response = $this->json('POST', 'register', $data);
        $response->assertStatus(422);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testUserRegisterWIthExistingUsername()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');

        $data = [
            'email'     => 'riskteria@gmail.com',
            'name'      => 'riskteria',
            'username'  => 'administrator',
            'password'  => 'password',
        ];

        $response = $this->json('POST', 'register', $data);
        $response->assertStatus(422);
        $response->json();
        $this->assertFalse(Auth::check());
    }

    public function testUserRegisterWIthNewAccount()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');

        $data = [
            'email'     => 'riskteria@gmail.com',
            'name'      => 'riskteria',
            'username'  => 'riskteria',
            'password'  => 'password',
        ];

        $response = $this->json('POST', 'register', $data);
        $response->assertStatus(201);
        $response->json();
        $this->assertTrue(Auth::check());
    }
}
