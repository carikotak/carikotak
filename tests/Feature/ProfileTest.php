<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeProfilePage()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');

        $user = User::first();

        $response = $this->get('/' . $user->username);
        $response->assertStatus(200);
    }
}
