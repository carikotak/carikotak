<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();
        $this->seed('CategoriesTableSeeder');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllCategories()
    {
        $response = $this->get('api/category');
        $response->json();
        $response->assertStatus(200);
    }

    public function testGetAllCategoriesWithProduct()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');
        $this->seed('MaterialSeeder');
        $this->seed('ProductsTableSeeder');

        $response = $this->get('api/category?with=product');
        $response->json();
        $response->assertStatus(200);
    }

    public function testGetAllCategoriesWithStories()
    {
        $this->seed('RolesTableSeeder');
        $this->seed('UsersTableSeeder');
        $this->seed('PostsTableSeeder');

        $response = $this->get('api/category?with=post');
        $response->json();
        $response->assertStatus(200);
    }
}
