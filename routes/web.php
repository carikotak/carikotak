<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::get('product/search', 'ProductController@create');

    Route::resource('product', 'ProductController',
        ['only' => ['create', 'show', 'edit']]);

    Route::resource('image', 'ImageController',
        ['only' => ['store']]);

    Route::resource('post', 'PostController',
        ['only' => ['create', 'edit']]
    );

    Route::resource('wishlist', 'WishlistController',
        ['only' => ['index']]
    );

    Route::resource('notification', 'NotificationController',
        ['only' => ['index']]
    );
});

Route::get('faq', 'FaqController@index');

Route::get('product/{slug}/{review?}', 'ProductController@show');

Route::get('faq', function () {
    return view('layouts.pages.general.faq');
});

Route::resource('settings/{route?}', 'SettingController',
    ['only' => ['index']]);

Route::resource('/', 'HomeController', [
    'only' => ['index']]);

Route::resource('product', 'ProductController',
    ['only' => ['index', 'show']]);

Route::resource('message', 'MessageController',
    ['only' => ['index', 'show']]);

Route::resource('search', 'SearchController',
    ['only' => ['index', 'show']]);

Route::resource('post', 'PostController',
    ['only' => ['index', 'show']]);

Route::group(['namespace' => 'Auth'], function () {

    Route::resource('login', 'LoginController',
        ['only' => ['index', 'store']]);

    Route::resource('oauth', 'OauthController',
        ['only' => 'store']);

    Route::resource('register', 'RegisterController',
        ['only' => ['index', 'store']]);

    Route::post('oauth/register', 'RegisterController@passport');

    Route::get('forgot-password', 'ForgotPasswordController@showResetLinkEmail');
    Route::get('forgot-password/{token}', 'ForgotPasswordController@showResetForm')->name('password.reset'); 
    Route::post('forgot-password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('forgot-password/reset', 'ForgotPasswordController@reset');

    Route::post('logout', 'LoginController@logout')->middleware('auth');
    Route::get('activation/{token}', 'RegisterController@verify');

});

Route::resource('token', 'TesController', ['only' => ['index']]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/{username}/{routing?}', 'UserController@show');