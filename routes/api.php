<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

    Route::resource('product', 'ProductController',
        ['only' => ['store', 'update', 'destroy']]
    );

    Route::resource('post', 'PostController',
        ['only' => ['store', 'update', 'destroy']]
    );

    Route::resource('me', 'MeController',
        ['only' => ['index', 'update', 'show']]
    );

    Route::resource('follow', 'FollowController',
        ['only' => ['update', 'destroy']]
    );

    Route::resource('favorite', 'FavoriteController',
        ['only' => ['update', 'destroy']]
    );

    Route::resource('comment', 'CommentController',
        ['only' => ['index', 'store', 'show', 'destroy']]
    );

    Route::resource('rating', 'RatingController',
        ['only' => ['store']]
    );

    Route::resource('channel', 'ChannelController',
        ['only' => ['index', 'show', 'store', 'destroy']]
    );

    Route::resource('message', 'MessageController',
        ['only' => ['index', 'show', 'store']]
    );

    Route::resource('notification', 'NotificationController',
        ['only' => ['index', 'show']]
    );

    Route::resource('report', 'ReportController',
        ['only' => ['store']]
    );


});

Route::resource('product', 'ProductController',
    ['only' => ['index', 'show']]
);

Route::resource('post', 'PostController',
    ['only' => ['index', 'show']]
);

Route::resource('category', 'CategoryController',
    ['only' => ['index', 'show']]
);

Route::resource('user', 'UserController',
    ['only' => ['index', 'show']]
);

Route::resource('location', 'LocationController',
    ['only' => ['index']]
);

Route::resource('register', 'RegisterController',
    ['only' => ['store']]
);

Route::resource('material', 'MaterialController',
    ['only' => ['index', 'show']]
);

Route::resource('image', 'ImageController',
    ['only' => ['store']]
);

Route::resource('tes', 'TesController');

Route::get('post/{post}/comment', 'PostController@getComment')->name('post.comment');
Route::get('product/{product}/comment', 'ProductController@getComment')->name('product.comment');

Route::get('user/{username}/follower', 'UserController@getFollower')->name('user.follower');
Route::get('user/{username}/following', 'UserController@getFollowing')->name('user.following');
Route::get('user/{username}/comment-product', 'UserController@getAllProductComment')->name('user.product.comment');
Route::get('user/{username}/comment-post', 'UserController@getAllPostComment')->name('user.post.comment');
Route::get('user/{username}/connection', 'UserController@getConnection')->name('user.connection');
