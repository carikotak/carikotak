<?php

namespace App\Models;

use App\Traits\Commentable\Commentable;
use App\Traits\Rateable\Rateable;
use App\Traits\Recommendable\Recommendable;
use App\Traits\Reportable\HasReport;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;

class Product extends Model
{
    use Sluggable, SluggableScopeHelpers, CanBeFavorited, Commentable, Rateable, Searchable, Recommendable, HasReport;

    protected $fillable = ['user_id', 'name', 'price', 'category_id', 'material_id', 'color',
                           'conditions', 'image', 'description', 'type', 'width', 'length', 'weight', 'height' ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        // $array = $this->toArray();

        $array = [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'type'          => $this->type
        ];

        return $array;
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function Material()
    {
        return $this->belongsTo(Material::class);
    }
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
