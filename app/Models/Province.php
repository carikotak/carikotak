<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public function User()
    {
        return $this->hasMany(User::class);
    }

    public function City()
    {
        return $this->hasMany(City::class);
    }

    public function Capital()
    {
        return $this->belongsTo(City::class, 'capital_city_id');
    }
}
