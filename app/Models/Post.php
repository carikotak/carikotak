<?php

namespace App\Models;

use App\Traits\Commentable\Commentable;
use App\Traits\Rateable\Rateable;
use App\Traits\Recommendable\Recommendable;
use App\Traits\Reportable\HasReport;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;

class Post extends Model
{
    use Sluggable, SluggableScopeHelpers, CanBeFavorited, Commentable, Rateable, Searchable, Recommendable, HasReport;

    protected $fillable = ['user_id', 'category_id', 'title', 'seo-title', 'excerpt', 'body',
        'image', 'slug', 'meta-description', 'meta-keywords', 'status', 'features'];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        // $array = $this->toArray();

        $array = [
            'id'        => $this->id,
            'title'     => $this->title,
            'body'      => $this->body
        ];

        return $array;
    }
    
    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
