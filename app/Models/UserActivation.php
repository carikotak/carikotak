<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    protected $fillable = ['token'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
