<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['body', 'user_id', 'reportable_id', 'reportable_type', 'title'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Reportable()
    {
        return $this->morphTo();
    }
}
