<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public function User()
    {
        return $this->hasMany(User::class);
    }

    public function Province()
    {
        return $this->belongsTo(Province::class);
    }
}
