<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Channel extends Model
{
    protected $fillable = ['name'];

    public function User()
    {
        return $this->belongsToMany(User::class)->withTimeStamps();
    }

    public function scopeIsJoinedChannel($query, User $user, User $target)
    {
        $isRelationshipExist = $user->Channel->intersect($target->Channel);
        return $isRelationshipExist;
    }

    public function Message()
    {
        return $this->hasMany(Message::class);
    }

    public function Participant()
    {
        return $this->hasMany(Participant::class);
    }
}
