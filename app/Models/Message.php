<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['text', 'user_id', 'channel_id'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
