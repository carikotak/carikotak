<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'content_id', 'content_type'
    ];

    /**
     * Rating belongs to a user.
     *
     * @return User
     */
    public function User()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */

    public function content()
    {
        return $this->morphTo();
    }

    /**
     * Gets users who commented on content,
     * except the user of this comment.
     *
     * Useful for notifying users about a new comment.
     *
     * @return Collection
     */
    public function getUsersWhoCommented()
    {
        $content_id     = $this->content->id;
        $content_type   = $this->content()->first()->getMorphClass();
        $user_id        = $this->user->id;
        return $this
            ->where('content_id', $content_id)
            ->where('content_type', $content_type)
            ->where('user_id', '!=', $user_id)
            ->get()
            ->pluck('user')
            ->unique();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }
    
}
