<?php

namespace App\Models;

use App\Traits\Reportable\HasReport;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Overtrue\LaravelFollow\Traits\CanFavorite;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, CanFollow, CanBeFollowed, CanFavorite, Searchable, HasReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'province_id', 'city_id', 'avatar', 'description', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [
            'id'            => $this->id,
            'name'          => $this->name,
            'username'      => $this->username,
            'email'         => $this->email
        ];

        return $array;
    }

    public function findForPassport($identifier)
    {
        return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
    }

    public function Product()
    {
        return $this->hasMany(Product::class);
    }

    public function Post()
    {
        return $this->hasMany(Post::class);
    }

    public function Channel()
    {
        return $this->belongsToMany(Channel::class)->withTimeStamps();
    }

    public function Message()
    {
        return $this->hasMany(Message::class);
    }

    public function SocialAuth()
    {
        return $this->hasOne(SocialAuth::class);
    }

    public function Province()
    {
        return $this->belongsTo(Province::class);
    }

    public function City()
    {
        return $this->belongsTo(City::class);
    }

    public function Activation()
    {
        return $this->hasOne(UserActivation::class);
    }

    public function Report()
    {
        return $this->hasMany(Report::class);
    }

    public function verified()
    {
        $this->confirmed = 1;
        $this->Activation->delete();
        $this->save();
    }
}
