<?php

namespace App\Exceptions;

use Exception;
use ReflectionClass;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Session\TokenMismatchException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) { 
            return static::handleErrorResponseJson($exception); 
        }
        return parent::render($request, $exception);

    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }

    private static function handleErrorResponseJson(Exception $exception)
    {
        switch(true) {

            case $exception instanceof ModelNotFoundException:

                $reflector = new ReflectionClass($exception->getModel());

                return response()->json([
                    'status'    => 'error',
                    'message'   => $reflector->getShortName() . ' not found'
                ], 404);

            case $exception instanceof AuthorizationException:

                return response()->json([
                    'status'    => 'error',
                    'message'   => 'this action is not authorized'
                ], 401);

            case $exception instanceof ValidationException:

                return response()->json($exception->getResponse()->original, 422);

            case $exception instanceof TokenMismatchException:

                return response()->json([
                    'status'    => 'error',
                    'message'   => 'token mismatch'
                ], 422);

            default:
                return response()->json([
                    'status'    => 'error',
                    'message'   => $exception->getMessage() ?: 'Something\'s wrong'
                ], 500);

        }
    }
}
