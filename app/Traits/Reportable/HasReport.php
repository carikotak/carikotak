<?php

/*
 * This file is part of Laravel Reportable.
 *
 * (c) Brian Faust <hello@brianfaust.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Traits\Reportable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use App\Models\Report;

trait HasReport
{
    public function reports(): MorphMany
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    public function report($data, Model $reportable): Report
    {
        $report = (new Report())->fill(array_merge($data, [
            'reportable_id' => $reportable->id,
            'reportable_id' => get_class($reportable),
        ]));

        $this->reports()->save($report);

        return $report;
    }
}