<?php

namespace App\Traits\Profileable;

trait UsernameGenerator {

    protected $username;

    private function generateUsername ($name) {
        
        $name = implode(explode(' ', $name));

        $time = round(microtime(true));

        $name_substr = substr($name, 0, 10);

        $unique_user_string = strtolower($name . $time);

        $this->username = strtolower($name_substr) . crc32($unique_user_string);

        return $this->username;
    }

}