<?php

namespace App\Traits\Commentable;

use App\Models\Comment;

trait Commentable {

    /**
     * This model has many ratings.
     *
     * @return Rating
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'content');
    }

}