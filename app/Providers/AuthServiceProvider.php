<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Product;

use App\Policies\CommentPolicy;
use App\Policies\PostPolicy;
use App\Policies\ProductPolicy;

use Carbon\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Comment::class  => CommentPolicy::class,
        Post::class     => PostPolicy::class,
        Product::class  => ProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addDays(15));

        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        Passport::enableImplicitGrant();
    }
}
