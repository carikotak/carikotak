<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class PostedNewStory extends Notification
{
    use Queueable;

    protected $story;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($story, User $user)
    {
        $this->story = $story;
        $this->user  = $user;
    }

    /***
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'story'   => $this->story,
            'user'    => $this->user
        ];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'story'   => $this->story,
            'user'    => $this->user
        ]);
    }
}
