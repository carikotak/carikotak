<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class PostedNewProduct extends Notification
{
    use Queueable;

    protected $product;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($product, User $user)
    {
        $this->product = $product;
        $this->user    = $user;
    }

    /***
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'product'   => $this->product,
            'user'      => $this->user
        ];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'product'   => $this->product,
            'user'      => $this->user
        ]);
    }
}
