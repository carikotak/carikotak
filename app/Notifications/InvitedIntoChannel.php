<?php

namespace App\Notifications;

use App\Models\Channel;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitedIntoChannel extends Notification implements ShouldQueue
{
    use Queueable;

    protected $channel;
    
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Channel $channel, User $user)
    {
        $this->channel  = $channel;
        $this->user     = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'channel'   => $this->channel,
            'user'      => $this->user
        ];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'channel'   => $this->channel,
            'user'      => $this->user
        ]);
    }
}
