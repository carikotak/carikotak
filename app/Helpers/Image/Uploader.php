<?php

namespace App\Helpers\Image;

use Illuminate\Http\Request;
use Storage;
use Auth;
use Log;
use JsonSerializable;

class Uploader extends ImageManager implements JsonSerializable
{

    private $image;

    private $drive;

    private $user = null;


    private $attributeFile = [];

    public function __construct($drive = "public")
    {

        $this->drive = $drive;

    }

    public function store($requestImage) {

        $this->image = $requestImage;

        if (!Auth::guest()) {

            $this->userFile();
        }


        return $this->storeToDisk();

    }

    public function storeToDisk() {

        $options = [
            "disk" => $this->drive,
            "visibility" => "public"
        ];

        $store = $this->image->storeAs("/original", $this->hashName(), $options);

        if ($store) {
            $this->storeAttribute();
            return;
        }
    }

    private function storeAttribute () {

        $this->attributeFile['ext'] = $this->image->clientExtension();

    }


    private function userFile() {

        $this->user = Auth::user();

    }

    private function hashName() {

        $baseFileName = $this->image->hashName() . " " . strtolower(str_random(10));

        $hashFileName = str_slug($baseFileName) . "." . $this->image->clientExtension();

        $this->attributeFile['filename'] = $hashFileName;

        return $hashFileName;
    }

    public function jsonSerialize() {

        return $this->attributeFile;

    }

    public function getAttribute($key)
    {

        return $this->attributeFile[$key];

    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __toString() {

        return $this->jsonSerialize();
    }

}
