<?php

namespace App\Helpers\Image;


use Storage;
use Image;
use App\Helpers\Image\ImageManager;
use Illuminate\Http\Response as IlluminateResponse;


class AutoResize extends ImageManager
{

    protected $lifetime = 43200;

    private $image;

    private $filename;

    private $path;

    public function parse($filename) {

        $this->path = public_path($this->getOriginalPath() . "/" . $filename);

    }

    public function render($w = null, $h = null) {


        try {

            $path = $this->path;

            $image = Image::cache(function($image) use ($path, $w, $h) {

                $image->make($path);

                if ($w & $h) {
                    $image->fit($w, $h);
                }


            }, 43200);
            return $this->buildResponse($image);

        } catch (\Intervention\Image\Exception\NotReadableException $e) {

            $this->image = null;
            return;
        }

    }


    # Source : https://github.com/Intervention/imagecache/blob/master/src/Intervention/Image/ImageCacheController.php
    #
    private function buildResponse($content)
    {

        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $content);
        return new IlluminateResponse($content, 200, array(
            'Content-Type' => $mime,
            'Cache-Control' => 'max-age='. ($this->lifetime * 60) .', public',
            'Etag' => md5($content),
        ));
    }

}
