<?php


namespace App\Helpers\Image;

use Illuminate\Http\Request;
use App\Helpers\Image\Uploader;

trait UploaderTrait {


    public function uploadImage(Request $request) {

        $drive = env('STORAGE_DRIVE', "public");

        $image = new Uploader($drive);

        $image->store($request->image);

        return response()->json(['filename' => $image->filename,'status' => 'ok'], 200);
    }
}
