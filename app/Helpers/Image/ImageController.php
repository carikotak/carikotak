<?php

namespace App\Helpers\Image;

use App\Jobs\StoreCacheImage;
use Carbon\Carbon;
use Config;
use Intervention\Image\ImageManager;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Log;
use Storage;
use Image;
use Intervention\Image\Exception\NotReadableException;

class ImageController extends BaseController
{

    use DispatchesJobs;

    protected $filename;

    protected $w;

    protected $h;

    protected $cachedFilename;

    private $disk;

    private $storage;

    private $imageManager;


    function __construct()
    {
        $this->disk = env('STORAGE_DRIVE', "public");

        $this->storage = Storage::disk($this->disk);

        $this->imageManager = new ImageManager(Config::get('image'));

   }


    public function handleAspectRatio($w, $filename) {

        $path = $this->storage->exists("original/{$filename}");

        if ($this->disk == "s3") {

            $path = $this->storage->url("original/{$filename}");

        } else {

            $path = $this->getImagePath($filename);

        }

        if ($path) {

            $manager = $this->imageManager;

            $content = $manager->cache(function ($image) use ($w,$path) {

                $image->make($path)->resize($w, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            }, config('imagecache.lifetime'));

            return $this->buildResponse($content);
        }
    }

	public function getReqeuest($w, $h, $filename, Request $request){

        if ($request->original) {

            return $this->handleOriginal($w, $h, $filename);
        }

        $this->filename = $filename;
        $this->w = $w;
        $this->h = $h;

        $this->cachedFilename = "{$w}_{$h}_{$filename}";

        if ($this->cachedExists($w, $h, $filename)) {

            // Redirect function
            return $this->redirecToCDN();
        }


        $pathOnS3 = "original/{$filename}";

        if(env('STORAGE_DRIVE', "public") == "s3") {

            $path = Storage::disk("s3")->url($pathOnS3);


        } else {

            $path = Storage::disk("public")->get($pathOnS3);
        }

        if ($path) {
            $manager = $this->imageManager;


            try {

                $content = $manager->cache(function ($image) use ($w, $h, $path) {
                    $image->make($path)->fit($w, $h);
                }, config('imagecache.lifetime'));

                // $this->storeToQueue($manager, $path);

                return $this->buildResponse($content);

            } catch (NotReadableException $e) {

                return $e->getMessage();

            }
        }

	}

    protected function handleOriginal($w, $h, $filename) {

        $path = $this->storage->exists("original/{$filename}");

        if ($this->disk == "s3") {

            $path = $this->storage->url("original/{$filename}");

        } else {

            $path = $this->getImagePath($filename);

        }

        if ($path) {

            $manager = $this->imageManager;

            $content = $manager->cache(function ($image) use ($w, $h, $path) {

                $image->make($path)->resize(null, $h, function ($constraint) {
                    $constraint->aspectRatio();
                });

            }, config('imagecache.lifetime'));

            return $this->buildResponse($content);
        }
    }

    private function redirecToCDN() {

        $cdn = env('CDN_URL', url('storage/cached'));

        $filename = $this->cachedFilename;

        //return $filename;

        $url = "{$cdn}/{$filename}";

        return redirect($url, 302);

    }

    private function storeToQueue(ImageManager $manager, String $path){

        $filename = $this->filename;

        $job = (new StoreCacheImage($this->w, $this->h, $this->filename,$path))
        ->delay(Carbon::now()->addSeconds(2));

        dispatch($job);
    }

    private function cachedExists($w, $h, $filename, $getOriginal = false) {


        if ($getOriginal) {
            return Storage::disk('public')->exists("cached/{$w}_{$h}_o_{$filename}");
        }

        return Storage::disk('public')->exists("cached/{$w}_{$h}_{$filename}");

    }

    /**
     * Find file
     * @param  String $filename filename
     * @return String Image path
     */
	private function getImagePath($filename)
    {
        foreach (config('imagecache.paths') as $path) {
            // don't allow '..' in filenames
            $image_path = $path.'/'.str_replace('..', '', $filename);
            if (file_exists($image_path) && is_file($image_path)) {
                // file found
                return $image_path;
            }
        }
        // file not found
        abort(404);
    }


    private function buildResponse($content)
    {
        // define mime type
        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $content);

        $etag = md5($content);
        $not_modified = isset($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] == $etag;
        $content = $not_modified ? NULL : $content;
        $status_code = $not_modified ? 304 : 200;

        return new IlluminateResponse($content, $status_code, array(
            'Content-Type' => $mime,
            'Cache-Control' => 'max-age='.(config('imagecache.lifetime')*60).', public',
            'Etag' => md5($content),
            'Expires' => gmdate('D, d M Y H:i:s \G\M\T', time() + 86400 * 365)
        ));
    }

}
