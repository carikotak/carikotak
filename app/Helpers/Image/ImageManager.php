<?php


namespace App\Helpers\Image;

use JsonSerializable;

class ImageManager
{

    private $diskName = "public";

    private $originalPath = "photo/original";

    private $attribute = [];


    public function setDiskName (String $diskName) {

        $this->diskName = $diskName;
    }

    public function getDiskname () {

        return $this->diskName;

    }

    public function setOriginalPath (String $originalPath) {

        $this->originalPath = $originalPath;
    }

    public function getOriginalPath() {

        return $this->originalPath;
    }

}
