<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class RatingTransformer extends AbstractTransformer
{ 
    public function transformModel(Model $rating)
    {
        $arr = [
            'id'                => $rating->id,
            'rating'            => $rating->rating,
            'user'              => $rating->User,
            'content'           => $rating->content,
            'created_at'        => $rating->created_at,
            'average_rating'    => $rating->content->averageRating
        ];

        return $arr;
    }

}
