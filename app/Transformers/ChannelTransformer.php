<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ChannelTransformer extends AbstractTransformer
{ 
    public function transformModel(Model $channel)
    {
        $options    = collect(@$this->options);

        $viewer     = $options['viewer'];

        $arr        = [
            'id'            => $channel->id,
            'name'          => $channel->name,
            'participants'  => $channel->User,
            'lastMessage'   => MessageTransformer::transform($channel->Message->last()),
            'messageTotal'  => $channel->Message()->count(),
            'communicant'   => $channel->User->where('id', '!=', $viewer->id)->first()
        ];

        if ($options->contains('message')) {
            $order = $options['order'] ?? 'desc';

            $arr['messages'] = MessageTransformer::transform($channel->Message()->orderBy('created_at', $order)->get());
        }

        return $arr;
    }

}
