<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class PostTransformer extends AbstractTransformer
{
    /**
     * @param Model $post
     * @return array
     */
    public function transformModel(Model $post)
    { 
        $arr = [
            'id'                => $post->id,
            'user'              => UserTransformer::transform($post->User),
            'category'          => $post->Category,
            'title'             => $post->title,
            'excerpt'           => $post->excerpt,
            'body'              => $post->body,
            'image'             => explode(',', $post->image),
            'slug'              => $post->slug,
            'status'            => $post->status,
            'rating'            => $post->averageRating,
            'created_at'        => $post->created_at->format('Ymd his'),
            'updated_at'        => $post->updated_at->format('Ymd his'),
            'total_favoriters'  => $post->favoriters()->count(),
            'total_comments'    => $post->comments()->count()
        ];

        if (Auth::guard('api')->check()) {

            $user = Auth::guard('api')->User();

            $arr['favorited']   = $post->isFavoritedBy($user);
            $arr['user_rating'] = $post->ratings()->where('user_id', $user->id)->avg('rating') ?? 0;

        }

        return $arr;
    }
}
