<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class MaterialTransformer extends AbstractTransformer
{
    public function transformModel(Model $material)
    {
        $arr    = [
            'id'         => $material->id,
            'label'      => $material->label,
            'products'   => ProductTransformer::transform($material->Product)
        ];

        return $arr;
    }

}