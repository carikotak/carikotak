<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class CategoryTransformer extends AbstractTransformer
{
    public function transformModel(Model $category)
    {
        $options    = collect(@$this->options);

        $arr = [
            'id'        => $category->id,
            'name'      => $category->name,
            'slug'      => $category->slug
        ];

        if ($options['type'] === 'product') {
            $arr['product'] = ProductTransformer::transform($category->Product->take(6));
        }

        if ($options['type'] === 'post') {
            $arr['post'] = PostTransformer::transform($category->Post->take(6));
        }

        return $arr;

    }
}