<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ProductCommentTransformer extends AbstractTransformer
{
    /**
     * @param Model $product
     * @return array
     */
    public function transformModel (Model $product)
    {
        $arr = [
            'id'        => $product->id,
            'name'      => $product->name,
            'slug'      => $product->slug,
            'image'     => explode(',', $product->image),
            'comment'   => $product->comments()->with('user')->get()
        ];

        return $arr;
    }
}