<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class PostCommentTransformer extends AbstractTransformer
{
    /**
     * @param Model $post
     * @return array
     */
    public function transformModel (Model $post)
    {
        $arr = [
            'id'        => $post->id,
            'title'     => $post->title,
            'slug'      => $post->slug,
            'image'     => $post->image,
            'comment'   => $post->comments()->with('user')->get()
        ];

        return $arr;
    }
}