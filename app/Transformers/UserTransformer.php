<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class UserTransformer extends AbstractTransformer
{
    public function transformModel (Model $user)
    {
        $currentUser = Auth::guard('api')->User();

        $arr = [
            'id'                => $user->id,
            'username'          => $user->username,
            'name'              => $user->name,
            'email'             => $user->email,
            'description'       => $user->description,
            'avatar'            => $user->avatar,
            'confirmed'         => (Boolean) $user->confirmed,
            'province'          => $user->Province,
            'city'              => $user->City,
            'phone'             => $user->phone,
            'total_product'     => $user->Product()->count(),
            'total_post'        => $user->Post()->count(),
            'total_followers'   => $user->followers()->count(),
            'total_followings'  => $user->followings()->count(),
            'created_at'        => $user->created_at->format('Ymd his')
        ];

        if ($currentUser != null && $user->id != $currentUser->id) {
            $arr['isFollowedByMe']      = $user->isFollowedBy($currentUser);
            $arr['isFollowingMe']       = $user->isFollowing($currentUser);
        }
        
        if ($currentUser != null && $user->id == $currentUser->id) {
            $arr['unreadNotification']  = $user->unreadNotifications()->count();
        }

        return $arr;

    }
}