<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class CommentTransformer extends AbstractTransformer
{ 
    public function transformModel(Model $comment)
    {
        $arr = [
            'id'            => $comment->id,
            'comment'       => $comment->comment,
            'user'          => $comment->User,
            'content_id'    => $comment->content_id,
            'create_at'     => $comment->created_at,
            'update_at'     => $comment->updated_at
        ];

        return $arr;
    }

}
