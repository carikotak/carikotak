<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class MessageTransformer extends AbstractTransformer
{ 
    public function transformModel(Model $message)
    {
        $arr = [
            '_id'           => $message->id,
            'text'          => $message->text,
            'user'          => [
                '_id'       => $message->User->id,
                'name'      => $message->User->name,
                'username'  => $message->User->username,
                'avatar'    => $message->User->avatar,
                'email'     => $message->User->email
            ],
            'channel'       => $message->Channel,
            'createdAt'    => $message->created_at->format('Y-m-d h:i:s')
        ];

        return $arr;
    }
}