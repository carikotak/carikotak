<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Auth;

class ProductTransformer extends AbstractTransformer
{
    /**
     * @param Model $product
     * @return array
     */
    public function transformModel (Model $product)
    { 
        $arr = [
            'id'                => $product->id,
            'user'              => UserTransformer::transform($product->User),
            'name'              => $product->name,
            'slug'              => $product->slug,
            'category'          => $product->Category,
            'material'          => $product->Material,
            'color'             => explode(',', strtolower($product->color)),
            'price'             => $product->price,
            'image'             => explode(',', $product->image),
            'description'       => $product->description,
            'type'              => $product->type,
            'height'            => $product->height,
            'width'             => $product->width,
            'length'            => $product->length,
            'conditions'        => $product->conditions,
            'rating'            => $product->averageRating,
            'created_at'        => $product->created_at->format('Ymd his'),
            'updated_at'        => $product->updated_at->format('Ymd his'),
            'total_favoriters'  => $product->favoriters()->count(),
            'total_comments'    => $product->comments()->count(),
            'total_ratings'     => $product->ratings()->count(),
        ];

        if (Auth::guard('api')->check()) {

            $user = Auth::guard('api')->User();

            $arr['favorited']   = $product->isFavoritedBy($user);
            $arr['user_rating'] = $product->ratings()->where('user_id', $user->id)->avg('rating') ?? 0;

        }

        return $arr;

    }
}