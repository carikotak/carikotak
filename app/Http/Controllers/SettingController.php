<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SettingController extends Controller
{
    public function index () {
        $viewer = Auth::user();
        return View('layouts.pages.auth.settings')->with('data', $viewer);
    }

    public function show ($id) {
        return View('layouts.pages.auth.settings');
    }
}
