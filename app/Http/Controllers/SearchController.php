<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SearchController extends Controller
{
    public function index()
    {
        Auth::check()
            ? $view = view('layouts.pages.auth.search')
            : $view = view('layouts.pages.general.search');

        $viewer = Auth::user();

        return $view->with('data', $viewer);
    }

    public function show($id)
    {
        Auth::check()
            ? $view = view('layouts.pages.auth.search')
            : $view = view('layouts.pages.general.search');

        return $view;
    }
}
