<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::check()
        ? $view = view('layouts.pages.auth.story')
        : $view = view('layouts.pages.general.story');

        $viewer = Auth::user();

        return $view->with('data', $viewer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::check()
        ? $view = view('layouts.pages.auth.article')
        : $view = redirect('/login');

        $viewer = Auth::user();

        return $view->with('data', $viewer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::findBySlugOrFail($slug);
        $viewer = Auth::user();

        Auth::check()
            ? $view = view('layouts.pages.auth.detail')
            : $view = view('layouts.pages.general.detail');

        return $view->with('data', $viewer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::findBySlugOrFail($slug);

        $this->authorize('update', $post);

        $viewer = Auth::user();

        return view('layouts.pages.auth.edit-article')->with('data', $viewer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
