<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class WishlistController extends Controller
{
    public function index () {
        $viewer = Auth::user();

        return view('layouts.pages.auth.wishlist')->with('data', $viewer);
    }
}
