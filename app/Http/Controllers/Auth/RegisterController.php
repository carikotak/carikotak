<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailVerification;
use App\Models\User;
use App\Models\UserActivation;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserAccount;
use Carbon\Carbon;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;
use Log;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'username'  => $data['username'],
            'password'  => bcrypt($data['password']),
        ]);
    }

    /**
     * Show register page.
     *
     * @return view
     */
    public function index()
    {
        return View('layouts.pages.general.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @return json
     */
    public function store(StoreUserAccount $request)
    {
        $body = $request->only('name', 'username', 'email', 'password');
        
        DB::beginTransaction();

        try {
            $user = $this->create($body);

            // $this->makeAccountActivation($user);

            $loginAttempt = Auth::login($user);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'account has been created',
                'url'       => url('/')
            ];

            return response()->json($response, 201);
        } catch (Exception $e) {
            DB::rollBack();

						return $e->getMessage();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'cannot create new user'
            ];

            return response()->json($response, 500);
        }
    }

    private function makeAccountActivation(User $user)
    {
        $token          = hash_hmac("sha1", $user->email, env('APP_KEY'));

        $activation     = new UserActivation(['token' => $token]);
        $email          = new EmailVerification($user, $token);

        $when           = Carbon::now()->addSeconds(1);

        $user->Activation()->save($activation);

        Mail::to($user->email)->later($when, $email);
    }

    public function verify(Request $request, $token)
    {
        $activationToken = UserActivation::whereToken($token)->firstOrFail();
        $user            = $activationToken->User;

        DB::beginTransaction();

        try {

            $user->verified();
            DB::commit();
            return redirect('login');

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            abort(404);
        }
    }
}
