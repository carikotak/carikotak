<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;
use Exception;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required|min:6',
        ]);
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    /**
     * Show login page.
     *
     * @return view
     */
    public function index()
    {
        return View('layouts.pages.general.login');
    }

    /**
     * Create new session for user.
     *
     * @return json
     */
    public function store(Request $request)
    {
        $body      = $request->only('email', 'password');

        $remember  = $request->remember ?? false;

        $validator = $this->validator($body);

        if ($validator->fails()) {
            $response = [
                'status'    => 'error',
                'message'   => 'Harap lengkapi form'
            ];
            return response()->json($response, 400);
        }

        $loginAttempt = Auth::attempt($body, $remember);

        if (!$loginAttempt) {
            $loginAttempt = Auth::attempt(['username' => $request->email, 'password' => $request->password], $remember);
        }

        if ($loginAttempt) {
            $response = [
                'status'    => 'ok',
                'message'   => 'login success',
                'url'       => url('/')
            ];

            return response()->json($response, 200);
        }

        $response = [
            'status'    => 'error',
            'message'   => 'Username atau password tidak cocok'
        ];

        return response()->json($response, 401);
    }

    /**
     * End user current session.
     *
     * @return void
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        $response = [
            'status'    => 'logout',
            'url'       => url('/login')
        ];

        return response()->json($response, 200);
    }

    public function social(Request $request)
    {
        $provider = $request->provider;

        $existUser = User::where('email', $request->email)->first();

        switch ($provider) {
            case 'facebook':
                //
                break;
            
            case 'google':
                //
                break;

            default:
                return response()->json(400);
        }
    }
}
