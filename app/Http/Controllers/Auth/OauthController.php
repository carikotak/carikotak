<?php

namespace App\Http\Controllers\Auth;

use App\Traits\Profileable\UsernameGenerator;
use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;
use Log;
use Validator;

class OauthController extends Controller
{
    use UsernameGenerator;

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all())->validate();

        $user = User::where('email', $request->email);

        $response = $user->exists()
                        ? $this->doLoginUser($request, $user)
                        : $this->doRegisterUser($request);

        return $response;
    }

    private function doLoginUser(Request $request, $user)
    {
        $user = $user->first();

        // if ($user->provider != $request->provider || $user->social_id != $request->social_id) {
        //     $response = [
        //         'status'    => 'error',
        //         'message'   => 'email has been used'
        //     ];

        //     return response()->json($response, 400);
        // }
        
        Auth::login($user);

        $response = [
            'status'    => 'ok',
            'message'   => 'login success',
            'url'       => url('/')
        ];

        return response()->json($response, 200);
    }

    private function doRegisterUser(Request $request)
    {
        DB::beginTransaction();

        try {
            $user = $this->createUser($request->all());

            Auth::login($user);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e);

            $response = [
                'status'    => 'error',
                'message'   => 'cannot create new user'
            ];

            return response()->json($response, 422);
        }
        
        $response = [
            'status'    => 'ok',
            'message'   => 'registration success',
            'url'       => url('/')
        ];

        return response()->json($response, 201);
    }

    private function createUser(array $data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'username'  => $this->generateUsername($data['name']),
            'password'  => bcrypt(str_random(10)),
            'provider'  => $data['provider'],
            'social_id' => $data['social_id']
        ]);
    }

    private function validator(array $data)
    {
        return Validator::make($data, [
            'email'         => 'required|email',
            'name'          => 'required',
            'provider'      => 'required',
            'social_id'     => 'required'
        ]);
    }
}
