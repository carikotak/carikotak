<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NotificationController extends Controller
{

    public function index () {
        $viewer = Auth::user();

        return view('layouts.pages.auth.notifications', ['data' => $viewer]);
    }
}
