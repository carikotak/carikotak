<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class FaqController extends Controller
{
    public function index () {
        $data = Auth::user();

        Auth::check()
            ? $view = view('layouts.pages.auth.faq')
            : $view = view('layouts.pages.general.faq');

        return $view->with('data', $data);
    }
}
