<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function index () {
        Auth::check()
        ? $view = view('layouts.pages.auth.profile')
        : $view = view('layouts.pages.general.profile');

        return $view;
    }

		public function show () {
        Auth::check()
        ? $view = view('layouts.pages.auth.profile')
        : $view = view('layouts.pages.general.profile');

        return $view;
		}
}
