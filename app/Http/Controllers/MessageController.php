<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class MessageController extends Controller
{
    public function index () {

      $viewer = Auth::user();

      Auth::check()
        ? $view = view('layouts.pages.auth.message')->with('data', $viewer)
        : $view = redirect('/login');

      return $view;
    }

    public function show ($id) {

      $viewer = Auth::user();

      Auth::check()
        ? $view = view('layouts.pages.auth.message')->with('data', $viewer)
        : $view = redirect('/login');

      return $view;
    }
}
