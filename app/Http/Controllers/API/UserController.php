<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Transformers\UserTransformer;
use App\Transformers\PostCommentTransformer;
use App\Transformers\ProductCommentTransformer;


use Illuminate\Http\Request; 

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->query('keyword');
        $perPage = $request->query('per_page');

        $users   = $keyword ? User::search($keyword) : User::orderByDesc('created_at');

        $users->paginate($perPage);

        $users   = $users->get();
        $userTransform = UserTransformer::transform($users);

        return response()->json($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $username)
    {
        $user          = User::whereUsername($username)->firstOrFail(); 
        $userTransform = UserTransformer::transform($user);
        
        return response()->json($userTransform, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllProductComment($username)
    {
        $user       = User::whereUsername($username)->firstOrFail();
        $comments   = ProductCommentTransformer::transform($user->Product);

        return response()->json($comments, 200);
    }

    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllPostComment($username)
    {
        $user       = User::whereUsername($username)->firstOrFail();
        $comments   = PostCommentTransformer::transform($user->Post);

        return response()->json($comments, 200);
    }

    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFollower($username)
    {
        $user       = User::whereUsername($username)->firstOrFail();
        $followers  = $user->followers()->get();
            
        return response()->json([
            'status'    => 'ok',
            'message'   => 'list of follower',
            'follower'  => UserTransformer::transform($followers)
        ]);
    }

    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFollowing($username)
    {
        $user       = User::whereUsername($username)->firstOrFail();
        $followings = $user->followings()->get();

        return response()->json([
            'status'    => 'ok',
            'message'   => 'list of following',
            'following' => UserTransformer::transform($followings)
        ], 200);
    }

    public function getConnection($username)
    {
        $user       = User::whereUsername($username)->firstOrFail();

        $followings = $user->followings()->get();
        $followers  = $user->followers()->get();

        $connection = $followings->merge($followers);

        return response()->json([
            'status'     => 'ok',
            'message'    => 'list of connection',
            'connection' => UserTransformer::transform($connection)
        ]);

    }
}