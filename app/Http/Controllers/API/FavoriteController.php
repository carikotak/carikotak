<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use App\Models\Product;
use App\Models\User;

use App\Http\Controllers\Controller;
use App\Notifications\AddedToFavorite;
use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;
use Log;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   = $request->user();
        $type   = $request->query('type') ?? 'product';

        $target = $type === 'post' ? Post::findOrFail($id) : Product::findOrFail($id);

        DB::beginTransaction();

        try { 

            $user->favorite($target);

            $notificationReceiver = $target->User;
            $notificationReceiver->notify(new AddedToFavorite($target, $user));

            DB::commit();

            $response = [
                'status'    => 'ok',
                'messsage'  => $type . ' added to favorite'
            ]; 

            return response()->json($response);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'cannot be favorited'
            ];

            return response()->json($response, 422);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user   = $request->user();
        $type   = $request->query('type') ?? 'product';

        $target = $type === 'post' ? Post::findOrFail($id) : Product::findOrFail($id);

        try { 

            $user->unfavorite($target);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'messsage'  => $type . ' removed from favorite'
            ]; 

            return response()->json($response);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => $type . 'cannot be unfavorited'
            ];

            return response()->json($response, 422);

        }

    }
}
