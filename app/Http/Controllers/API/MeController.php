<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserAccount;
use App\Models\User;
use App\Models\Post;
use App\Models\Product;
use App\Request\ChangePassword;
use App\Transformers\UserTransformer;

use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;
use Hash;
use Log;
use Validator;

class MeController extends Controller
{
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user          = $request->user(); 
        $userTransform = UserTransformer::transform($user);

        return response()->json($userTransform, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $type)
    {
        $user = $request->User();

        switch ($type) {

            case 'followers':
                $followers  = $user->followers()->get();
                return response()->json(UserTransformer::transform($followers), 200);

            case 'followings':
                $followings = $user->followings()->get();
                return response()->json(UserTransformer::transform($followings), 200);

            case 'favorite-posts':
                $favorites  = $user->favorites(Post::class)->get();
                return response()->json($favorites->load(['user']), 200);

            case 'favorite-products':
                $favorites  = $user->favorites(Product::class)->get();
                return response()->json($favorites->load(['user']), 200);

            case 'connection':
                $followings = $user->followings()->get();
                $followers  = $user->followers()->get();
                $connection = $followings->merge($followers);

                return response()->json(UserTransformer::transform($connection), 200);

            default:
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'page not found'
                ], 404);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type)
    {
        switch ($type) {

            case 'update-profile':
                return $this->updateProfile($request);

            case 'change-password':
                return $this->changePassword($request);

            default:
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'page not found'
                ], 404);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateProfile ($request) {
        $user = $request->User();
        $data = $request->only(['name', 'email', 'city_id', 'province_id', 'avatar', 'description', 'phone']);

        $validator = Validator::make($data, [
            'name'          => 'required',
            'email'         => 'required',
            'city_id'       => 'nullable|exists:cities,id',
            'province_id'   => 'nullable|exists:provinces,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 'error',
                'message'   => $validator->getMessageBag()
            ], 422);
        }

        if ($request->email != $user->email) {
            $validateEmail = Validator::make($data, [
                'email' => 'required|unique:users,email'
            ]);

            if ($validateEmail->fails()) {
                return response()->json([
                    'status'    => 'error',
                    'message'   => $validateEmail->getMessageBag()
                ], 422);
            }
        }

        DB::beginTransaction();

        try {
            $user->update($data);

            DB::commit();

            return response()->json([
                'status'    => 'ok',
                'message'   => 'profile updated successfully',
                'user'      => $user
            ], 200);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            return response()->json([
                'status'    => 'error',
                'message'   => 'cannot update profile'
            ], 422);
        }


    }

    public function changePassword ($request) {
        $user = $request->User();
        $data = $request->all();

        $validator = Validator::make($data, [
            'current_password'  => 'required',
            'new_password'      => 'required|same:new_password',
            'confirm_password'  => 'required|same:new_password'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 'error',
                'message'   => $validator->getMessageBag()
            ], 422);
        }
        
        $passwordMatch = Hash::check($request->current_password, $user->password);

        if (!$passwordMatch) {
            return response()->json([
                'status'    => 'error',
                'message'   => 'please enter correct current password'
            ], 422);
        }

        DB::beginTransaction();

        try {

            $user->password = Hash::make($request->new_password);
            $user->save();

            DB::commit();

            return response()->json([
                'status'    => 'ok',
                'message'   => 'password changed successfully'
            ], 200);

        } catch (Exception $e) {
            
            DB::rollBack();

            Log::error($e->getMessage());

            return response()->json([
                'status'    => 'error',
                'message'   => 'cannot change password'
            ], 422);
        }
        

    }
}
