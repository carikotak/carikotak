<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProduct;
use App\Models\Product;
use App\Notifications\PostedNewProduct;
use App\Transformers\ProductTransformer;
use App\Transformers\CommentTransformer;

use Illuminate\Http\Request;
use Notification;

use Broadcast;
use DB;
use Exception;
use Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $userId     = $request->query('user');
        $get        = $request->query('get');
        $keyword    = $request->query('keyword');
        $perPage    = $request->query('per_page') ?? 15;
        $location   = $request->query('location');
        $width      = $request->query('width');
        $height     = $request->query('height');
        $weight     = $request->query('weight');
        $length     = $request->query('length');
        $category   = $request->query('category');
        $condition  = $request->query('condition');
        $material   = $request->query('material');
        $province   = $request->query('province');
        $city       = $request->query('city');

        $product = null;

        if ($province) {
            $product = Product::whereHas('user', function ($q) use ($province) {
                $q->where('province_id', $province);
            })->searchable();
        }

        if ($city) {
            $product = Product::whereHas('user', function ($q) use ($city) {
                $q->where('city_id', $city);
            })->searchable();
        }

        if (!$product) {
            $product = $keyword ? Product::search($keyword) : Product::orderByDesc('created_at');
        } else {
            $product = $keyword ? $product->search($keyword) : $product->orderByDesc('created_at');
        }


        if ($userId) {
            $product->where('user_id', $userId);
        }

        switch ($get) {

            case 'popular':
                break;

            case 'trending':
                break;

            default:
                break;

        }

        if ($condition == 'new' || $condition == 'second') {
            $product->where('conditions', $condition);
        }

        if ($material) {
            $product->where('material_id', $material);
        }

        if ($category) {
            $product->where('category_id', $category);
        }

        $product->paginate($perPage);

        $product          = $product->get();
        $productTransform = ProductTransformer::transform($product);
        
        return response()->json($productTransform, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        $user = $request->user();
        $data = $request->all();

        $data['image'] = implode(',', $request->image ?? ['']);
        $data['color'] = implode(',', $request->color ?? ['']);

        DB::beginTransaction();

        try { 

            $product          = $user->Product()->create($data); 
            $productTransform = ProductTransformer::transform($product);

            $followers = $user->followers()->get();
            
            Notification::send($followers, new PostedNewProduct($productTransform, $user));

            DB::commit();

            $response = [
                'status'    => 'ok',
                'product'   => $productTransform,
                'message'   => 'product has been added'
            ];

            return response()->json($response, 201);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());
            
            return $e;

            $response = [
                'status'    => 'error',
                'message'   => 'product cannot be added'
            ];

            return response()->json($response, 400);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product          = Product::findBySlugOrFail($slug);
        $productTransform = ProductTransformer::transform($product);

        return response()->json($productTransform, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProduct $request, Product $product)
    {
        $this->authorize('update', $product);

        $user = $request->user();
        $data = $request->all();

        $data['image'] = implode(',', $request->image ?? ['']);
        $data['color'] = implode(',', $request->color ?? ['']);

        DB::beginTransaction();

        try {

            $product->update($data);
            $productTransform = ProductTransformer::transform($product);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'product has been updated',
                'product'   => $productTransform
            ];

            return response()->json($response, 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'product cannot be updated'
            ];

            return response()->json($response, 400); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        DB::beginTransaction();

        try {

            $product->delete(); 

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'product deleted successfully'
            ];

            return response()->json($response, 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'product cannot be deleted'
            ];

            return response()->json($response, 400);

        }
    }

    public function getComment (Product $product) {
        $comments           = $product->comments()->orderBy('created_at', 'desc')->get();
        $commentTransform   = CommentTransformer::transform($comments);
        
        return response()->json($commentTransform, 200);
    }
}
