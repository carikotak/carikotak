<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Notifications\InvitedIntoChannel;
use App\Notifications\ThreadReplied;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMessage;

use App\Transformers\MessageTransformer;

use App\Models\Channel;
use App\Models\Message;
use App\Models\User;

use DB;
use Exception;
use Log;
use Notification;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMessage $request)
    {
        $user       = $request->User();
        $target     = User::findOrFail($request->user_id);

        $channel    = Channel::IsJoinedChannel($user, $target);
        $hasJoined  = (Boolean) $channel->count();

        DB::beginTransaction();

        try {

            if (!$hasJoined) {

                $channel = $user->Channel()->create(['name' => 'unnamed']);
                $channel->User()->attach($target);

                $target->notify(new InvitedIntoChannel($channel, $user));

            } else {
                $channel = $channel[0];
            }

            $message = $user->Message()->create([
                'channel_id'    => $channel->id,
                'text'          => $request->text
            ]);

            $messageTransform = MessageTransformer::transform($message);
            
            $users   = $channel->User->except(['id' => $user->id]);
            Notification::send($users, new ThreadReplied($messageTransform, $user));

            DB::commit();

            return response()->json([
                'status'    => 'ok',
                'message'   => $messageTransform
            ], 201);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e);

            return response()->json([
                'status'    => 'error',
                'message'   => 'cannot sent the message'
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $this->authorize('delete', $message);

        DB::beginTransaction();

        try {

            $message->delete();

            DB::commit();

            return response()->json([
                'status'    => 'ok',
                'message'   => 'message has been deleted'
            ], 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            return response()->json([
                'status'    => 'error',
                'message'   => 'cannot delete message'
            ], 422);
        }
    }
}
