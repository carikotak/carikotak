<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\NewFollower;
use Illuminate\Http\Request;

use Auth;
use DB;
use Exception;
use Log;

class FollowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $follow)
    {
        $user = $request->User();

        DB::beginTransaction();

        try {

            $user->follow($follow);

            $follow->notify(new NewFollower($user));

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'user is followed'
            ]; 

            return response()->json($response, 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'cannot be followed'
            ];

            return response()->json($response, 422);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $follow)
    {
        $user = $request->user();

        DB::beginTransaction();

        try {

            $user->unfollow($follow);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'user is unfollowed'
            ];

            return response()->json($response, 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'user cannot be unfollowed'
            ];

            return response()->json($response, 422);

        }
    }
}
