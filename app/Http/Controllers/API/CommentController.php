<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreComment; 
use App\Models\Comment;
use App\Models\Post;
use App\Models\Product;
use App\Notifications\PostCommented;
use App\Transformers\CommentTransformer;

use Illuminate\Http\Request;

use DB;
use Exception;
use Log;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $user           = $request->User();
        $target_id      = $request->target_id;
        $target_type    = strtolower($request->target_type);

        $target         = $target_type === 'post' ? Post::findOrFail($target_id) : Product::findOrFail($target_id);

        DB::beginTransaction();

        try {
            
            $comment = $target->comments()->create([
                'user_id'   => $user->id,
                'comment'   => $request->comment
            ]);

            $notificationReceiver = $target->User;

            $notificationReceiver->notify(new PostCommented($comment, $user));

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'comment has been posted',
                'comment'   => CommentTransformer::transform($comment)
            ];

            return response()->json($response, 201);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e);

            $response = [
                'status'    => 'error',
                'message'   => 'cannot post comment'
            ];

            return response()->json($response, 422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $comment = CommentTransformer::transform($comment);
        return response()->json($comment, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComment $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);

        DB::beginTransaction();

        try {

            $comment->delete();

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'comment has been deleted'
            ];

            return response()->json($response, 200);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'cannot delete comment'
            ];

            return response()->json($response, 422);
        }
    }
}
