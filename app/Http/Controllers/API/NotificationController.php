<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->User();
        $get  = $request->query('get');

        switch ($get)
        {
            case 'all':
                $notifications = $user->notifications()->orderBy('created_at', 'desc')->get();
                break;

            default:
                $notifications = $user->unreadNotifications()->orderBy('created_at', 'desc')->get();
                break;

        }

        foreach ($notifications as $notification) {
            $notification->markAsRead();
        }

        $notifications = collect($notifications)->flatten(1)->unique('data');
        $notifications = array_values($notifications->toArray());

        return response()->json($notifications, 200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $type)
    {
        $user = $request->User();

        $notificationsTotal = $user->notifications()->count();

        return response()->json([
            'status'    => 'ok',
            'message'   => $notificationsTotal
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
