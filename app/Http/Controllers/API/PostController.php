<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePost;
use App\Models\Post;
use App\Notifications\PostedNewStory;
use App\Transformers\PostTransformer;
use App\Transformers\CommentTransformer;
use App\Traits\Searchable\Filterable;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Notification;

use Broadcast;
use DB;
use Exception;
use Log;

class PostController extends Controller
{
    use Filterable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId  = $request->query('user');
        $get     = $request->query('get');
        $keyword = $request->query('keyword');
        $perPage = $request->query('per_page') ?? 15;
        $category = $request->query('category');

        $post    = $keyword ? Post::search($keyword) : Post::orderByDesc('created_at');

        if ($userId) {
            $post->where('user_id', $userId);
        }
        
        switch ($get) {

            case 'popular':
                break;

            case 'trending':
                break;

            default:
                break;

        }

        if ($category) {
            $post->where('category_id', $category);
        }
        
        $post->paginate($perPage);

        $post           = $post->get();
        $postTransform  = PostTransformer::transform($post);
        
        return response()->json($postTransform, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $user = $request->user();
        $data = $request->all();

        DB::beginTransaction();

        try {
            $post          = $user->Post()->create($data); 
            $postTransform = PostTransformer::transform($post);

            $followers = $user->followers()->get();

            Notification::send($followers, new PostedNewStory($postTransform, $user));

            DB::commit();

            $response = [
                'status'    => 'ok',
                'post'      => $postTransform,
                'message'   => 'post has been added'
            ];

            return response()->json($response, 201);   

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'post cannot be added'
            ];

            return response()->json($response, 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post           = Post::findBySlugOrFail($slug);
        $postTransform  = PostTransformer::transform($post);
        
        return response()->json($postTransform, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, Post $post)
    {
        $this->authorize('update', $post);

        $user = $request->user();
        $data = $request->all();

        $data['image'] = implode(',', $request->image);

        DB::beginTransaction();

        try {

            $post->update($data);
            $postTransform = PostTransformer::transform($post);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'Post has been updated',
                'post'      => $postTransform
            ];

            return response()->json($response, 200);
            
        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'post cannot be updated'
            ];

            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        
        DB::beginTransaction();

        try {

            $post->delete(); 

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'post has been removed successfully'
            ];

            return response()->json($response, 200);
            
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'post cannot be deleted'
            ];

            return response()->json($response, 400);
        }
    }

    public function getComment (Post $post) {
        $comments           = $post->comments()->orderBy('created_at', 'desc')->get();
        $commentTransform   = CommentTransformer::transform($comments);
        
        return response()->json($commentTransform, 200);
    }
}
