<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;
use Laravel\Passport\Client;

use DB;
use Exception;
use Log;
use Route;
use Validator;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'username'  => $data['username'],
            'password'  => bcrypt($data['password']),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $body = $request->only('name', 'username', 'email', 'password');

        $validator = Validator::make($body, [
            'name'      => 'required|max:255',
            'username'  => 'required|max:20|unique:users',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 422);
        }
        
        DB::beginTransaction();

        try {
            $user   = $this->create($body);

            DB::commit();
            
            $client = Client::where('password_client', 1)->first();

            $request->request->add([
                'grant_type'    => 'password',
                'client_id'     => $client->id,
                'client_secret' => $client->secret
            ]);

            $request->offsetUnset('name', 'email');


            $proxy = Request::create('oauth/token', 'POST');

            return Route::dispatch($proxy);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e);

            $response = [
                'status'    => 'error',
                'message'   => 'cannot create new user'
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
