<?php

namespace App\Http\Controllers\API;

use App\Models\Channel;
use App\Models\Participant;
use App\Models\User;
use App\Notifications\InvitedIntoChannel;
use App\Http\Controllers\Controller;
use App\Transformers\ChannelTransformer;
use Illuminate\Http\Request;

use DB;
use Exception;
use Log;
use Auth;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user               = $request->User();
        $channels           = $user->Channel;
        $channelTransforms  = ChannelTransformer::transform($channels, ['viewer' => $user]);

        return response()->json($channelTransforms, 200);
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = $request->User();
        $data       = $request->only('name');
        $target     = User::findOrFail($request->user_id);

        $channel    = Channel::IsJoinedChannel($user, $target);
        
        $hasJoined  = (Boolean) $channel->count();

        DB::beginTransaction();

        try {

            if (!$hasJoined) {

                $channel = $user->Channel()->create($data);
                $channel->User()->attach($target);

                $target->notify(new InvitedIntoChannel($channel, $user));

            } else {
                $channel = $channel[0];
            }


            DB::commit();

            $channelTransform = ChannelTransformer::transform($channel, ['viewer' => $user]);

            return response()->json($channelTransform, 200);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            return response()->json([
                'status'    => 'error',
                'message'   => 'cannot get or create channel'
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Channel $channel)
    {
        $user               = $request->User();
        $order              = $request->query('order');
        
        $channelTransform   = ChannelTransformer::transform($channel, ['message' => true, 'viewer' => $user, 'order' => $order]);
        
        return response()->json($channelTransform, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Channel $channel)
    {
        $user = $request->User();

        DB::beginTransaction();

        try {

            $channel->User()->detach($user);

            DB::commit();

            return response()->json([
                'status'    => 'ok',
                'message'   => 'channel has been removed successfully'
            ], 200);

        } catch (Exception $e) {

            DB::rollBack();

            Log::error($e->getMessage());

            return response()->json([
                'status'    => 'error',
                'message'   => 'channel cannot be created'
            ], 400);

        }
    }
}
