<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRating;
use App\Models\Rating;
use App\Models\Post;
use App\Models\Product;
use App\Transformers\RatingTransformer;

use Illuminate\Http\Request;

use DB;
use Exception;
use Log;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRating $request)
    {
        $user           = $request->User();
        $target_id      = $request->target_id;
        $target_type    = strtolower($request->target_type);

        $target = $target_type === 'post' ? Post::findOrFail($target_id) : Product::findOrFail($target_id);

        DB::beginTransaction();

        try {

            $rating = $target->ratings()->updateOrCreate(
            [ 'user_id'   => $user->id, 'content_id' => $target->id, 'content_type' => get_class($target) ],
            [ 'rating'    => $request->rating, 'user_id'   => $user->id ]);

            DB::commit();

            $response = [
                'status'    => 'ok',
                'message'   => 'rated success',
                'rating'    => RatingTransformer::transform($rating)
            ];

            return response()->json($response, 200);

        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            $response = [
                'status'    => 'error',
                'message'   => 'cannot be rated'
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRating $request, $type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        // 
    }
}
