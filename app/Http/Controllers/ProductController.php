<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::check()
        ? $view = View('layouts.pages.auth.product')
        : $view = View('layouts.pages.general.product');

        $viewer = Auth::user();

        return $view->with('data', $viewer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = view('layouts.pages.auth.create');
        $viewer = Auth::user();

        return $view->with('data', $viewer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, $review = null)
    {
        $product = Product::findBySlugOrFail($slug);
        $viewer = Auth::user();

        Auth::check()
            ? $view = view('layouts.pages.auth.product')
            : $view = view('layouts.pages.general.product');

        return $view->with([
            'data' => $viewer,
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $product = Product::findBySlugOrFail($slug);

        $this->authorize('update', $product);

        $viewer = Auth::user();

        return view('layouts.pages.auth.edit-product')->with('data', $viewer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
