import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLazyload from 'vue-lazyload'
import App from 'authComponent/message/message.vue'
import PusherEcho from 'lib/pusher-echo-plugin'
import AxiosPlugin from 'lib/axios-plugin'
import store from '../store/message'
import notifyMe from 'lib/alert-notification'
import { squareMedium } from 'lib/image-ratio'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)
Vue.use(VueRouter)

/* Router view component */
import InboxView from 'authComponent/message/_partials/inbox-view.vue'

const router = new VueRouter({
    mode: 'history',
    base: '/message',
    routes: [
        {
            path: '/:id',
            name: 'inbox-view',
            component: InboxView
        }
    ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        // notifyMe(e, 'Notifikasi')
        store.dispatch('pushMessage', e.message)
    })


new Vue({
    el: '#app-container',
    components: {
        App
    },
    store,
    router
})
