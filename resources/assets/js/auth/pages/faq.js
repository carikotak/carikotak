import Vue from 'vue';
import App from 'authComponent/faq/faq.vue';

new Vue({
  el: '#app-container',
  components: {
    App
  }
});
