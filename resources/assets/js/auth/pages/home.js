import Vue from 'vue'
import AxiosPlugin from 'lib/axios-plugin'
import PusherEcho from 'lib/pusher-echo-plugin'
import App from 'authComponent/home/home.vue'
import store from '../store/home'
import VueLazyload from 'vue-lazyload'
import VueRouter from 'vue-router'
import {squareMedium} from 'lib/image-ratio'
import notifyMe from 'lib/alert-notification'

Vue.use(VueLazyload)
Vue.use(AxiosPlugin)
Vue.use(VueRouter)

const router = new VueRouter({mode: 'history'})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            console.log(e)
            notifyMe(e, 'Notifikasi')
            store.dispatch('pushNotification', e)
        }
    })

new Vue({el: '#app-container', components: {
        App
    }, store, router})
