import Vue from 'vue'
import AxiosPlugin from 'lib/axios-plugin'
import  App from 'authComponent/notification/notification.vue'
import store from '../store/page-notification'
import VueLazyload from 'vue-lazyload'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)

new Vue({
  el: '#app-container',
  components: {
    App
  },
  store
})
