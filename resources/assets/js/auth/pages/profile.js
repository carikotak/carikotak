import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import PusherEcho from 'lib/pusher-echo-plugin'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'authComponent/profile/profile.vue'
import store from '../store/profile'
import VueLazyload from 'vue-lazyload'
import notifyMe from 'lib/alert-notification'
import {squareMedium} from 'lib/image-ratio'

Vue.use(AxiosPlugin)
Vue.use(VueRouter)
Vue.use(VueLazyload)
Vue.use(VeeValidate)

/* Router view component */
import profileProduct from 'authComponent/profile/_partials/profile-product.vue'
import profileReview from 'authComponent/profile/_partials/profile-review.vue'
import profileStory from 'authComponent/profile/_partials/profile-story.vue'

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
            path: '/:username/',
            name: 'profile-product',
            component: profileProduct
        }, {
            path: '/:username/review',
            name: 'profile-review',
            component: profileReview
        }, {
            path: '/:username/story',
            name: 'profile-story',
            component: profileStory
        }
    ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi')
            store.dispatch('pushNotification', e)
        }
    })

new Vue({el: '#app-container', components: {
        App
    }, router, store})
