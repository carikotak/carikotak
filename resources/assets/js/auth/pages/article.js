import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueAxios from 'lib/axios-plugin'
import App from 'authComponent/article/article.vue'
import PusherEcho from 'lib/pusher-echo-plugin'
import { squareMedium } from 'lib/image-ratio'
import notifyMe from 'lib/alert-notification'
import store from '../store/article'

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

Vue.use(VueAxios)
Vue.use(VeeValidate)

new Vue({
    el: '#app-container',
    components: {
        App
    },
    store
})
