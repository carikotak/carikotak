import Vue from 'vue'
import VueRouter from 'vue-router'
import App from 'authComponent/settings/settings.vue'
import AxiosPlugin from 'lib/axios-plugin'
import PusherEcho from 'lib/pusher-echo-plugin'
import { squareMedium } from 'lib/image-ratio'
import notifyMe from 'lib/alert-notification'
import store from '../store/settings'

Vue.use(VueRouter)
Vue.use(AxiosPlugin)

/* Partial Components */
import AccountSettings from 'authComponent/settings/_partials/AccountSettings.vue'
import ProfileSettings from 'authComponent/settings/_partials/ProfileSettings.vue'

const router = new VueRouter({
  mode: 'history',
  base: '/settings',
  routes: [
    {
      path: '/',
      name: 'profile-settings',
      component: ProfileSettings
    },
    {
      path: '/account',
      name: 'account-settings',
      component: AccountSettings
    }
  ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
