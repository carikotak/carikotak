import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import PusherEcho from 'lib/pusher-echo-plugin'
import App from 'authComponent/product/product.vue'
import AxiosPlugin from 'lib/axios-plugin'
import store from '../store/product'
import VueLazyload from 'vue-lazyload'
import notifyMe from 'lib/alert-notification'
import { squareMedium } from 'lib/image-ratio'

Vue.use(AxiosPlugin)
Vue.use(VueRouter)
Vue.use(VueLazyload)
Vue.use(VeeValidate)

/* Router view */
import descriptionBox from 'authComponent/product/_partials/description-box.vue'
import reviewBox from 'authComponent/product/_partials/review-box.vue'

const router = new VueRouter({
  mode: 'history',
  base: '/product',
  routes: [
    {
      path: '/:slug',
      name: 'description-box',
      component: descriptionBox
    },
    {
      path: '/:slug/review',
      name: 'review-box',
      component: reviewBox
    }
  ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
