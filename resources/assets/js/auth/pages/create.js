import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import App from 'authComponent/create/create.vue'
import AxiosPlugin from 'lib/axios-plugin'
import PusherEcho from 'lib/pusher-echo-plugin'
import { squareMedium } from 'lib/image-ratio'
import notifyMe from 'lib/alert-notification'
import store from '../store/create'

Vue.use(AxiosPlugin)
Vue.use(VueRouter)
Vue.use(VeeValidate)

/* Partial Components */
import sellProduct from 'authComponent/create/_partials/sell-product.vue'
import searchProduct from 'authComponent/create/_partials/search-product.vue'

const router = new VueRouter({
  mode: 'history',
  base: '/product',
  routes: [
    {
      path: '/create',
      name: 'sell-product',
      component: sellProduct
    },
    {
      path: '/search',
      name: 'search-product',
      component: searchProduct
    }
  ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
