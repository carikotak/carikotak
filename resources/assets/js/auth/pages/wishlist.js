import Vue from 'vue'
import AxiosPlugin from 'lib/axios-plugin'
import  App from 'authComponent/wishlist/wishlist.vue'
import store from '../store/wishlist'
import VueLazyload from 'vue-lazyload'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)

new Vue({
  el: '#app-container',
  components: {
    App
  },
  store
})
