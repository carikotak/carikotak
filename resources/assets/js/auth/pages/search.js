import Vue from 'vue'
import AxiosPlugin from 'lib/axios-plugin'
import PusherEcho from 'lib/pusher-echo-plugin'
import VueRouter from 'vue-router'
import App from 'authComponent/search/search.vue'
import store from '../store/search'
import VueLazyload from 'vue-lazyload'
import notifyMe from 'lib/alert-notification'
import { squareMedium } from 'lib/image-ratio'

Vue.use(VueRouter)
Vue.use(AxiosPlugin)
Vue.use(VueLazyload)

import SearchStory from 'authComponent/search/_partials/_search-story'
import SearchSell from 'authComponent/search/_partials/_search-sell'
import SearchUser from 'authComponent/search/_partials/_search-user'

const router = new VueRouter({
  mode: 'history',
  base: '/search',
  routes: [
    {
      path: '/',
      name: 'search-sell',
      component: SearchSell
    },
    {
      path: '/story',
      name: 'search-story',
      component: SearchStory
    },
    {
      path: '/user',
      name: 'search-user',
      component: SearchUser
    }
  ]
})

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
