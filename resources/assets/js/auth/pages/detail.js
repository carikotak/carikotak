import Vue from 'vue'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'authComponent/detail/detail.vue'
import store from '../store/detail'
import PusherEcho from 'lib/pusher-echo-plugin'
import VueLazyload from 'vue-lazyload'
import notifyMe from 'lib/alert-notification'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    store
})
