import Vue from 'vue'
import VueAxios from 'lib/axios-plugin'
import VeeValidate from 'vee-validate'
import App from 'authComponent/edit-article/edit-article.vue'
import store from '../store/edit-article/'
import PusherEcho from 'lib/pusher-echo-plugin'
import { squareMedium } from 'lib/image-ratio'
import notifyMe from 'lib/alert-notification'

Vue.use(VueAxios)
Vue.use(VeeValidate)

PusherEcho
    .channel(`private-App.Models.User.${window._sharedData.id}`)
    .listen('BroadcastNotificationCreated', (e) => {
        if (e.type === "App\\Notifications\\ThreadReplied") {
            store.dispatch('pushNotifMessage', e)
        } else {
            notifyMe(e, 'Notifikasi') 
            store.dispatch('pushNotification', e)
        }
    })

new Vue({
    el: '#app-container',
    components: {
        App
    },
    store
})
