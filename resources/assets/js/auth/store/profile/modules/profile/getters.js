export const profile = state => state.profile
export const product = state => state.product
export const story = state => state.story
export const comment = state => state.comment
export const viewer = state => state.viewer
export const follower = state => state.follower
export const following = state => state.following
