export default {

  SETUP_PROFILE (state, shared) {
    state.profile = shared
  },

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  REMOVE_PRODUCT (state, id) {
    state.product.splice(id, 1)
  },

  GATHER_STORY (state, data) {
    state.story = data
  },

  REMOVE_STORY (state, id) {
    state.story.splice(id, 1)
  },

  REMOVE_COMMENT (state, data) {
    state.comment[data.index].comment.splice(data.localIndex, 1)
    // state.comment.splice(id, 1)
  },

  GATHER_COMMENT (state, data) {
    state.comment = data
  },

  GATHER_VIEWER (state, shared) {
    state.viewer = shared
  },

  GATHER_FOLLOWER (state, data) {
    state.follower = data
  },

  GATHER_FOLLOWING (state, data) {
    state.following = data
  },

  PUSH_FOLLOWER (state, data) {
    state.follower.push(data)
  },

  PULL_FOLLOWER (state, data) {
    const indexMe = state.follower.findIndex(i => i.id === data.id)

    state.follower.splice(indexMe, 1)
  }
}
