import * as types from './mutation-types'

export const setupProfile = ({ commit }, shared) => {
  commit(types.SETUP_PROFILE, shared)
}

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const removeProduct = ({ commit }, id) => {
  commit(types.REMOVE_PRODUCT, id)
}

export const gatherStory = ({ commit }, data) => {
  commit(types.GATHER_STORY, data)
}

export const removeStory = ({ commit }, data) => {
  commit(types.REMOVE_STORY, data) }

export const gatherComment = ({ commit }, data) => {
  commit(types.GATHER_COMMENT, data)
}

export const removeComment = ({ commit }, id) => {
  commit(types.REMOVE_COMMENT, id)
}

export const gatherViewer = ({ commit }, shared) => {
  commit(types.GATHER_VIEWER, shared)
}

export const gatherFollower = ({ commit }, data) => {
  commit(types.GATHER_FOLLOWER, data)
}

export const gatherFollowing = ({ commit }, data) => {
  commit(types.GATHER_FOLLOWING, data)
}

export const pushFollower = ({ commit }, data) => {
  commit(types.PUSH_FOLLOWER, data)
}

export const pullFollower = ({ commit }, data) => {
  commit(types.PULL_FOLLOWER, data)
}
