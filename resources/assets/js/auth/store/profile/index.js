import Vue from 'vue'
import profile from './modules/profile'
import notification from '../notification'

const Vuex = require('vuex').default

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    profile,
    notification
  }
})