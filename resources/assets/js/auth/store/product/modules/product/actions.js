import * as types from './mutation-types'

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const gatherUser = ({ commit }, data) => {
  commit(types.GATHER_USER, data)
}

export const gatherComment = ({ commit }, data) => {
  commit(types.GATHER_COMMENT, data)
}

export const gatherViewer = ({ commit }, shared) => {
  commit(types.GATHER_VIEWER, shared)
}

export const removeComment = ({ commit}, id) => {
  commit(types.REMOVE_COMMENT, id)
}

export const postComment = ({ commit }, data) => {
  commit(types.POST_COMMENT, data)
}
