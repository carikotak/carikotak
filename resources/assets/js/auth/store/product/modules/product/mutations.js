export default {

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  GATHER_USER (state, data) {
    state.user = data
  },

  GATHER_COMMENT (state, data) {
    state.comment = data
  },

  GATHER_VIEWER (state, shared) {
    state.viewer = shared
  },

  REMOVE_COMMENT (state, id) {
    state.comment.splice(id, 1)
  },

  POST_COMMENT (state, data) {
    state.comment.push(data)
  }
}
