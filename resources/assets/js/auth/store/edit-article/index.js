import Vue from 'vue'
import Vuex from 'vuex'
import editArticle from './modules/edit-article'
import notification from '../notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    editArticle,
    notification
  }
})