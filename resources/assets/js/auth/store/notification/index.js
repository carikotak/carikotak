import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  notification: [],
  notifMessage: []
}

const getters = {
  notification: state => state.notification,
  notifMessage: state => state.notifMessage
}

const actions = {

  gatherNotification({
    commit
  }, data) {
    commit('GATHER_NOTIFICATION', data)
  },

  pushNotification({
    commit
  }, data) {
    commit('PUSH_NOTIFICATION', data)
  },

  pushNotifMessage({
    commit
  }, data) {
    commit('PUSH_NOTIF_MESSAGE', data)
  }
}

const mutations = {

  GATHER_NOTIFICATION(state, data) {
    state.notification = data
  },

  PUSH_NOTIFICATION(state, data) {
    const notif = {
      data: {
        post: data.post || [],
        user: data.user || [],
        comment: data.comment || []
      },
      type: data.type,
      notifiable: data.user,
      notifiable_id: data.user.id
    }

    state
      .notification
      .push(notif)
  },

  PUSH_NOTIF_MESSAGE(state, data) {
    state
      .notifMessage
      .push(data)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}