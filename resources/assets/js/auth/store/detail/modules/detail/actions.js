import * as types from './mutation-types'

export const gatherUser = ({ commit }, data) => {
  commit(types.GATHER_USER, data)
}

export const gatherComment = ({ commit }, data) => {
  commit(types.GATHER_COMMENT, data)
}

export const gatherStory = ({ commit }, data) => {
  commit(types.GATHER_STORY, data)
}

export const postComment = ({ commit }, data) => {
  commit(types.POST_COMMENT, data)
}

export const gatherViewer = ({ commit }, data) => {
  commit(types.GATHER_VIEWER, data)
}

export const deleteComment = ({ commit }, id) => {
  commit(types.DELETE_COMMENT, id)
}

export const setLoading = ({ commit }, data) => {
  commit(types.SET_LOADING, data)
}