export default {

  GATHER_USER (state, data) {
    state.user = data
  },

  GATHER_COMMENT (state, data) {
    state.comment = data
  },

  GATHER_STORY (state, data) {
    state.story = data
  },

  POST_COMMENT (state, data) {
    state.comment.push(data)
  },

  DELETE_COMMENT (state, id) {
    state.comment.splice(id, 1)
  },

  GATHER_VIEWER (state, data) {
    state.viewer = data
  },

  SET_LOADING (state, data) {
    state.loading = data
  }

}
