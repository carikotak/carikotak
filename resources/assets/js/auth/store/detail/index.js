import Vue from 'vue'
import Vuex from 'vuex'
import detail from './modules/detail'
import notification from '../notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    detail,
    notification
  }
})