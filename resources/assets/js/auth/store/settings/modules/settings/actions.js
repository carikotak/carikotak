import * as types from './mutation-types'

export const gatherSettings = ({ commit }, data) => {
  commit(types.GATHER_SETTINGS, data)
}