import Vue from 'vue'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const Vuex = require('vuex').default

const state = {
  settings: []
}

Vue.use(Vuex)

export default {
  state,
  actions,
  getters,
  mutations
}