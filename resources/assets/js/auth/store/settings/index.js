import Vue from 'vue'
import settings from './modules/settings'
import notification from '../notification'

const Vuex = require('vuex').default

export default new Vuex.Store({
  modules: {
    settings,
    notification
  }
})