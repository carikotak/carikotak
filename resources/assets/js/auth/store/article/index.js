import Vue from 'vue'
import article from './modules/article'
import notification from '../notification'

const Vuex = require('vuex').default

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    article,
    notification
  }
})