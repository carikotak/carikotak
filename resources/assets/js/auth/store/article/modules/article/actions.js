import * as types from './mutation-types'

export const gatherArticle = ({ commit }, data) => {
  commit(types.GATHER_ARTICLE, data)
}