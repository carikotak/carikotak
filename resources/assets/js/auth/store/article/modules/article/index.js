import Vue from 'vue'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const Vuex = require('vuex').default

const state = {
  article: []
}

Vue.use(Vuex)

export default {
  state,
  getters,
  actions,
  mutations
}