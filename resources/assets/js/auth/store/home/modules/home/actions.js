import * as types from './mutation-type'

export const initProduct = ({ commit }, data) => {
    commit(types.INIT_PRODUCT, data)
}

export const initStory = ({ commit }, data) => {
    commit(types.INIT_STORY, data)
}
