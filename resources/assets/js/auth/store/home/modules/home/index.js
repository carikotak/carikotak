import Vue from 'vue'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import Pusher from 'pusher-js'
import PusherEcho from 'lib/pusher-echo-plugin'

Window.Pusher = Pusher

window.Echo = PusherEcho

const Vuex = require('vuex').default

Vue.use(Vuex)

const state = {
  product: [],
  story: []
}

export default {
  state,
  mutations,
  actions,
  getters
}