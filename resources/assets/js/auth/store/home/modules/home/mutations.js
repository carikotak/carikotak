export default {

  INIT_PRODUCT (state, product) {
    state.product = product
  },

  INIT_STORY (state, story) {
    state.story = story
  }
}
