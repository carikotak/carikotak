import Vue from 'vue'
import Vuex from 'vuex'
import home from './modules/home'
import notification from '../notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    home,
    notification
  }
})