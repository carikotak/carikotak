import * as types from './mutation-types'

export const gatherWishlist = ({ commit }, data) => {
  commit(types.GATHER_WISHLIST, data)
}

export const removeWishlist = ({ commit }, data) => {
  commit(types.REMOVE_WISHLIST, data)
}

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const removeProduct = ({ commit }, data) => {
  commit(types.REMOVE_PRODUCT, data)
}

export const setupProfile = ({ commit }, shared) => {
  commit(types.SETUP_PROFILE, shared)
}
