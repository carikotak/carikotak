export default {

  GATHER_WISHLIST (state, data) {
    state.wishlist = data
  },

  REMOVE_WISHLIST (state, data) {
    state.wishlist.splice(data, 1)
  },

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  REMOVE_PRODUCT (state, data) {
    state.product.splice(data, 1)
  },

  SETUP_PROFILE (state, shared) {
    state.profile = shared
  }
}
