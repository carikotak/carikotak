import Vue from 'vue'
import Vuex from 'vuex'
import wishlist from './modules/wishlist'
import notification from '../notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    wishlist,
    notification
  }
})