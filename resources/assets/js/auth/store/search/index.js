import Vue from 'vue'
import Vuex from 'vuex'
import search from './modules/search'
import notification from '../notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    search,
    notification
  }
})