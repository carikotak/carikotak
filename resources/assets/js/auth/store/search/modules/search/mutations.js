export default {

  GATHER_ALL_PRODUCT (state, data) {
    state.product = data
  },

  GATHER_USER (state, data) {
    state.user = data
  },

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  GATHER_STORY (state, data) {
    state.story = data
  }
}
