import * as types from './mutation-types'

export const gatherAllProduct = ({ commit }, data) => {
  commit(types.GATHER_ALL_PRODUCT, data)
}

export const gatherUser = ({ commit }, data) => {
  commit(types.GATHER_USER, data)
}

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const gatherStory = ({ commit }, data) => {
  commit(types.GATHER_STORY, data)
}