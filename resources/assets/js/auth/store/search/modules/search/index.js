import Vue from 'vue'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const Vuex = require('vuex').default

Vue.use(Vuex)

const state = {
  product: [],
  story: [],
  user: []
}

export default {
  state,
  actions,
  getters,
  mutations
}
