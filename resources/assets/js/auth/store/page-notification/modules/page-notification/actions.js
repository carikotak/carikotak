import * as types from './mutation-types'

export const gatherPageNotification = ({ commit }, data) => {
  commit(types.GATHER_PAGE_NOTIFICATION, data)
}