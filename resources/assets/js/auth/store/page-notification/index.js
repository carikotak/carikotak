import Vue from 'vue'
import pageNotification from './modules/page-notification'
import notification from '../notification'

const Vuex = require('vuex').default

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    pageNotification,
    notification
  }
})