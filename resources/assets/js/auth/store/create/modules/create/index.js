import Vue from 'vue'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const Vuex = require('vuex').default

Vue.use(Vuex)

const state = {
  product: []
}

export default {
  state,
  getters,
  actions,
  mutations
}