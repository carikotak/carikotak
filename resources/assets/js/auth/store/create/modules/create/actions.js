import * as types from './mutation-types'

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}