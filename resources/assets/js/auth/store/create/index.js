import Vue from 'vue'
import create from './modules/create/'
import notification from '../notification'

const Vuex = require('vuex').default

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    create,
    notification
  }
})