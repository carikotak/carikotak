import Vue from 'vue'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'

const Vuex = require('vuex').default

Vue.use(Vuex)

const state = {
  story: []
}

export default {
  state,
  actions,
  getters,
  mutations
}
