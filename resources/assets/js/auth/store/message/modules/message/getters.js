export const channel = state => state.channel
export const message = state => state.message
export const lastMessage = state => state.lastMessage
export const viewer = state => state.viewer