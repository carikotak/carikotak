import Vue from 'vue'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'

const Vuex = require('vuex').default
Vue.use(Vuex)

const state = {
  channel: [],
  message: [],
  lastMessage: '',
  viewer: []
}

export default {
  state,
  actions,
  getters,
  mutations
}