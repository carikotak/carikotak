import * as types from './mutation-types'

export const gatherChannel = ({ commit }, data) => {
  commit(types.GATHER_CHANNEL, data)
}

export const pushChannel = ({ commit }, data) => {
  commit(types.PUSH_CHANNEL, data)
}

export const deleteChannel = ({ commit }, id) => {
  commit(types.DELETE_CHANNEL, id)
}

export const gatherMessage = ({ commit }, data) => {
  commit(types.GATHER_MESSAGE, data)
}

export const gatherViewer = ({ commit }, data) => {
  commit(types.GATHER_VIEWER, data)
}

export const pushMessage = ({ commit }, data) => {
  commit(types.PUSH_MESSAGE, data)
}

export const getLastMessage = ({ commit }, data) => {
  commit(types.GET_LAST_MESSAGE, data)
}