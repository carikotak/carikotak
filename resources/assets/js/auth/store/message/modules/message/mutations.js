export default {

  GATHER_CHANNEL (state, data) {
    state.channel = data
  },

  PUSH_CHANNEL (state, data) {
    state.channel.push(data)
  },

  DELETE_CHANNEL (state, id) {
    state.channel.splice(id, 1)
  },

  GATHER_MESSAGE (state, data) {
    state.message = data
  },

  GATHER_VIEWER (state, shared) {
    state.viewer = shared
  },

  PUSH_MESSAGE (state, data) {
    state.message.push(data)
  },

  GET_LAST_MESSAGE (state, data) {
    state.lastMessage = data
  }
}