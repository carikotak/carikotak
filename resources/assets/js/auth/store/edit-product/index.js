import Vue from 'vue'
import editProduct from './modules/edit-product'
import notification from '../notification'

const Vuex = require('vuex').default

Vue.use(Vuex)

export default new Vuex.Store ({
  modules: {
    editProduct,
    notification
  }
})