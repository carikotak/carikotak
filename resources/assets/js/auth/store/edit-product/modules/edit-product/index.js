import Vue from 'vuex'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const Vuex = require('vuex').default

const state = {
  article: []
}

export default {
  state,
  getters,
  actions,
  mutations
}