import { squareMedium } from 'lib/image-ratio'

export default function notifyMe(e, title) {
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  else if (Notification.permission === "granted") {
    if (e.type === "App\\Notifications\\AddedToFavorite") {
      const typed = e.post.name ? e.post.name : e.post.title
      const data = {
          body: `${e.user.name} memfavoritkan ${typed}`,
          icon: squareMedium + e.user.avatar
      }
      var notification = new Notification(title, data);
    }

    else if (e.type === "App\\Notifications\\PostCommented") {
      const typed = e.comment.content.name ? e.comment.content.name : e.comment.content.title
      const data = {
        body: `${e.user.name} mengomentari ${typed}`,
        icon: squareMedium + e.user.avatar 
      }
      var notification = new Notification(title, data);
    }

    else if (e.type === "App\\Notifications\\NewFollower") {
      const data = {
        body: `${e.user.name} mengikuti anda`,
        icon: squareMedium + e.user.avatar
      }
      var notification = new Notification(title, data)
    }

    else if ( e.type === "App\\Notifications\\PostedNewStory") {
      const data = {
        body: `${e.user.name} memposting cerita ${e.story.title}`,
        icon: squareMedium + e.user.avatar
      }
      var notification = new Notification(title, data)
    }

    else if ( e.type === "App\\Notifications\\PostedNewProduct") {
      const data = {
        body: `${e.user.name} memposting cerita ${e.product.name}`,
        icon: squareMedium + e.user.avatar
      }
      var notification = new Notification(title, data)
    }

    else {
    }
  }

  else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        var notification = new Notification(title, data);
      }
    })
  }
}