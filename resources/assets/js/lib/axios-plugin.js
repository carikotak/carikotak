import axios from 'axios'

axios.defaults.baseURL = window.location.origin
axios.defaults.timeout = 15000
axios.defaults.headers.common = {
  'X-CSRF-TOKEN': document.querySelector("meta[name='_csrf']").content,
  'X-Requested-With': 'XMLHttpRequest',
  'Accept': 'application/json'
}

export default (Vue) => {
  Object.defineProperties(Vue.prototype, {
    $http: {
      get () {
        return axios
      }
    }
  })
}
