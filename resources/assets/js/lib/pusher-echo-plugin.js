import Pusher from 'pusher-js'
import Echo from 'laravel-echo'

window.Pusher = Pusher

export default new Echo({
    broadcaster: 'pusher',
    key: window.pusherAppKey,
    cluster: window.pusherAppCluster,
    encrypted: true,
    authEndpoint: '/broadcasting/auth',
    namespace: 'Illuminate.Notifications.Events',
    auth: {
        headers: {
            'X-CSRF-Token': document.querySelector('meta[name=_csrf]').getAttribute('content')
        }
    }
})