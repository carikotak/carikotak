import Vue from 'vue';
import App from 'generalComponent/faq/faq.vue';

new Vue({
  el: '#app-container',
  components: {
    App
  }
});
