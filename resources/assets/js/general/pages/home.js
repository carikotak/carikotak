import Vue from 'vue'
import VeeValidate from 'vee-validate'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'generalComponent/home/home.vue'
import store from '../store/home'
import VueLazyload from 'vue-lazyload'
import VueRouter from 'vue-router'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)
Vue.use(VueRouter)
Vue.use(VeeValidate)

const router = new VueRouter({
    base: '/',
    mode: 'history'
})

new Vue({
    el: '#app-container',
    components: {
        App
    },
    store,
    router
})
