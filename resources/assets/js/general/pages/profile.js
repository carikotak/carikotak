import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'generalComponent/profile/profile.vue'
import store from '../store/profile'
import VueLazyload from 'vue-lazyload'

Vue.use(VueRouter)
Vue.use(AxiosPlugin)
Vue.use(VueLazyload)
Vue.use(VeeValidate)

/* Router view component */
import profileProduct from 'generalComponent/profile/_partials/profile-product.vue'
import profileReview from 'generalComponent/profile/_partials/profile-review.vue'
import profileStory from 'generalComponent/profile/_partials/profile-story.vue'

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/:username/',
      name: 'profile-product',
      component: profileProduct
    },
    {
      path: '/:username/review',
      name: 'profile-review',
      component: profileReview
    },
    {
      path: '/:username/story',
      name: 'profile-story',
      component: profileStory
    }
  ]
})


new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
