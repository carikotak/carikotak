import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'generalComponent/product/product.vue'
import store from '../store/product'
import VueLazyload from 'vue-lazyload'

Vue.use(AxiosPlugin)
Vue.use(VueRouter)
Vue.use(VueLazyload)
Vue.use(VeeValidate)

/* Router component */
import descriptionBox from 'generalComponent/product/_partials/description-box.vue'
import reviewBox from 'generalComponent/product/_partials/review-box.vue'

const router = new VueRouter({
  mode: 'history',
  base: '/product',
  routes: [
    {
      path: '/:slug',
      name: 'description-box',
      component: descriptionBox
    },
    {
      path: '/:slug/review',
      name: 'review-box',
      component: reviewBox
    }
  ]
})

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
