import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import VuePaginate from 'vue-paginate'
import App from 'generalComponent/search/search.vue'
import AxiosPlugin from 'lib/axios-plugin'
import store from '../store/search'
import VueLazyload from 'vue-lazyload'

Vue.use(VueRouter)
Vue.use(AxiosPlugin)
Vue.use(VueLazyload)
Vue.use(VeeValidate)
Vue.use(VuePaginate)

import SearchStory from 'generalComponent/search/_partials/_search-story'
import SearchSell from 'generalComponent/search/_partials/_search-sell'
import SearchUser from 'generalComponent/search/_partials/_search-user'

const router = new VueRouter({
  mode: 'history',
  base: '/search',
  routes: [
    {
      path: '/',
      name: 'search-sell',
      component: SearchSell
    },
    {
      path: '/story',
      name: 'search-story',
      component: SearchStory
    },
    {
      path: '/user',
      name: 'search-user',
      component: SearchUser
    }
  ]
})

new Vue({
    el: '#app-container',
    components: {
        App
    },
    router,
    store
})
