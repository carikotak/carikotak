import Vue from 'vue'
import VeeValidate from 'vee-validate'
import AxiosPlugin from 'lib/axios-plugin'
import App from 'generalComponent/detail/detail.vue'
import store from '../store/detail'
import VueLazyload from 'vue-lazyload'

Vue.use(AxiosPlugin)
Vue.use(VueLazyload)
Vue.use(VeeValidate)

new Vue({
    el: '#app-container',
    components: {
        App
    },
    store
})
