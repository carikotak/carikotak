import Vue from 'vue'
import VeeValidate from 'vee-validate'
import App from 'generalComponent/login/login.vue'
import AxiosPlugin from 'lib/axios-plugin'

Vue.use(AxiosPlugin)
Vue.use(VeeValidate)

new Vue({
    el: '#app-container',
    components: {
        App
    }
})
