import axios from 'axios'
import * as types from './mutation-types'

export const gatherStory = ({ commit }, data) => {
    commit(types.GATHER_STORY, data)
}
