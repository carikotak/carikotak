import * as types from './mutation-types'

export const setupProfile = ({ commit }, shared) => {
  commit(types.SETUP_PROFILE, shared)
}

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const gatherStory = ({ commit }, data) => {
  commit(types.GATHER_STORY, data)
}

export const gatherComment = ({ commit }, data) => {
  commit(types.GATHER_COMMENT, data)
}

export const gatherFollower = ({ commit }, data) => {
  commit(types.GATHER_FOLLOWER, data)
}

export const gatherFollowing = ({ commit }, data) => {
  commit(types.GATHER_FOLLOWING, data)
}
