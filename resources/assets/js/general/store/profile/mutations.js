export default {

  SETUP_PROFILE (state, shared) {
    state.profile = shared
  },

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  GATHER_STORY (state, data) {
    state.story = data
  },

  GATHER_COMMENT (state, data) {
    state.comment = data
  },

  GATHER_FOLLOWER (state, data) {
    state.follower = data
  },

  GATHER_FOLLOWING (state, data) {
    state.following = data
  }
}
