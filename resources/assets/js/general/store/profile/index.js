import Vue from 'vue'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'

const Vuex = require('vuex').default

Vue.use(Vuex)

const state = {
  profile: [],
  product: [],
  story: [],
  comment: [],
  follower: [],
  following: []
}

export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations
})
