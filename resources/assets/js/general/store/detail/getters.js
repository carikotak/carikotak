export const comment = state => state.comment
export const user = state => state.user
export const story = state => state.story
export const viewer = state => state.viewer
