import * as types from './mutation-types'

export const gatherProduct = ({ commit }, data) => {
  commit(types.GATHER_PRODUCT, data)
}

export const gatherUser = ({ commit }, data) => {
  commit(types.GATHER_USER, data)
}

export const gatherComment = ({ commit }, data) => {
  commit(types.GATHER_COMMENT, data)
}
