export default {

  GATHER_PRODUCT (state, data) {
    state.product = data
  },

  GATHER_USER (state, data) {
    state.user = data
  },

  GATHER_COMMENT (state, data) {
    state.comment = data
  }
}
