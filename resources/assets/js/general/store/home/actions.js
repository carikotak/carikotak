import axios from 'axios'
import * as types from './mutation-types'

export const initProduct = ({ commit }, data) => {
    commit(types.INIT_PRODUCT, data)
}

export const initStory = ({ commit }, data) => {
    commit(types.INIT_STORY, data)
}
