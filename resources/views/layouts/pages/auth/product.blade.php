@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{!! asset('local/alertifyjs/alertify.min.css') !!}">
    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/product.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/product.css') !!}">
    @endif
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        window._sharedData = {!! $data !!}
        window._product = {!! $product !!}
    </script>

    <script type="text/javascript">
        window.pusherAppKey = '{{ env('PUSHER_APP_KEY') }}'
        window.pusherAppCluster = '{{ env('PUSHER_APP_CLUSTER') }}'
    </script>
    
    <script src="{!! asset('local/id.js') !!}"></script>
    <script src="{!! asset('local/alertifyjs/alertify.min.js') !!}"></script>

    <script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>

    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/a/product.js') }}"></script>
    @else
        <script src="{!! asset('scripts/a/product.js') !!}"></script>
    @endif
@endsection
