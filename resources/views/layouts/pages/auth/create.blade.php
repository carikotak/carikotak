@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{!! asset('local/alertifyjs/alertify.min.css') !!}">
    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/create.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/create.css') !!}">
    @endif
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window._sharedData = {!! $data !!}
    </script>
    <script src="{!! asset('local/alertifyjs/alertify.min.js') !!}"></script>
    <script src="/local/jquery-3.1.1/dist/jquery.min.js"></script>
    <script type="text/javascript">
        window.pusherAppKey = '{{ env('PUSHER_APP_KEY') }}'
        window.pusherAppCluster = '{{ env('PUSHER_APP_CLUSTER') }}'
    </script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>
    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/a/create.js') }}"></script>

    @else

        <script src="{!! asset('scripts/a/create.js') !!}"></script>

    @endif
@endsection
