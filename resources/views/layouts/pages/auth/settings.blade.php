@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{!! asset('local/alertifyjs/alertify.min.css') !!}">
    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/settings.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/settings.css') !!}">
    @endif
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window._sharedData = {!! $data !!}
    </script>
    <script src="{!! asset('local/alertifyjs/alertify.min.js') !!}"></script>
    <script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlnSINjQzLOqGQC0htSXpYWwLGvuKYlSQ&v=3.exp&sensor=false&libraries=places"></script>
    <script>
        function init() {
            var input = document.getElementById('search-city');
            var autocomplete = new google.maps.places.Autocomplete(input);
        }

        google.maps.event.addDomListener(window, 'load', init);
    </script>

    <script type="text/javascript">
        window.pusherAppKey = '{{ env('PUSHER_APP_KEY') }}'
        window.pusherAppCluster = '{{ env('PUSHER_APP_CLUSTER') }}'
    </script>

    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/a/settings.js') }}"></script>
    @else
        <script src="{!! asset('scripts/a/settings.js') !!}"></script>
    @endif
@endsection
