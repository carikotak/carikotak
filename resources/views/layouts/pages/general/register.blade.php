@extends('layouts.master')

@section('styles')

    <link rel="stylesheet" href="{!! asset('local/alertifyjs/alertify.min.css') !!}">

    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/register.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/register.css') !!}">
    @endif
@endsection

@section('meta')
	<meta name="google-signin-scope" content="profile email">
	<meta name="google-signin-client_id" content="{{ env('GOOGLE_CLIENT_ID') }}" id="google-client-id">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')
	<script type="text/javascript">
		window.fbAsyncInit = function () {
			FB.init({
				appId: {{ env('FACEBOOK_APP_ID') }},
				xfbml: true,
				version: 'v2.8'
			});
			FB.AppEvents.logPageView();
		};

        (function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) { return };
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ env('GOOGLE_KEY') }}"></script>
    <script src="{!! asset('local/alertifyjs/alertify.min.js') !!}"></script>
    <script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>

    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/g/register.js') }}"></script>

    @else
        <script src="{!! asset('scripts/g/register.js') !!}"></script>
    @endif
@endsection
