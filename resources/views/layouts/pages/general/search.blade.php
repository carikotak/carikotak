@extends('layouts.master')

@section('styles')
    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/search.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/search.css') !!}">
    @endif
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')
		<script type="text/javascript">
			window.fbAsyncInit = function () {
				FB.init({
					appId: {{ env('FACEBOOK_APP_ID') }},
					xfbml: true,
					version:'v2.8'
				});
				FB.AppEvents.logPageView();
			};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
		</script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlnSINjQzLOqGQC0htSXpYWwLGvuKYlSQ&v=3.exp&sensor=false&libraries=places"></script>

    <script>
        function init() {
            var input = document.getElementById('search-city');
            var autocomplete = new google.maps.places.Autocomplete(input);
        }

        google.maps.event.addDomListener(window, 'load', init);
    </script>

		<script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
		<script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>

    @if (env('APP_ENV') === "production")

        <script src="{{ elixir('scripts/g/search.js') }}"></script>

    @else

        <script src="{!! asset('scripts/g/search.js') !!}"></script>

    @endif
@endsection
