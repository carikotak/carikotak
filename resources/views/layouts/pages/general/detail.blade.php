@extends('layouts.master')

@section('styles')
    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/detail.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/detail.css') !!}">
    @endif
@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>
    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/g/detail.js') }}"></script>
    @else
        <script src="{!! asset('scripts/g/detail.js') !!}"></script>

    @endif
@endsection
