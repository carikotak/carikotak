@extends('layouts.master')

@section('styles')

    <link rel="stylesheet" href="{!! asset('local/alertifyjs/alertify.min.css') !!}">

    @if (env('APP_ENV') === "production")
        <link rel="stylesheet" href="{{ elixir('css/faq.css') }}">
    @else
        <link rel="stylesheet" href="{!! asset('css/faq.css') !!}">
    @endif

@endsection

@section('contents')
    <div>
        <div id="app-container">
            <app></app>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{!! asset('local/alertifyjs/alertify.min.js') !!}"></script>
    <script src="{!! asset('local/jquery-3.1.1/dist/jquery.min.js') !!}"></script>
    <script src="{!! asset('local/bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>
    
    @if (env('APP_ENV') === "production")
        <script src="{{ elixir('scripts/g/faq.js') }}"></script>
    @else
        <script src="{!! asset('scripts/g/faq.js') !!}"></script>
    @endif
@endsection
