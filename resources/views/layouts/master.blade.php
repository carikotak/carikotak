<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="utf-8" />
    <meta name="HandheldFriendly" content="true"/>
    <meta name="_csrf" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    {{-- Behavioral Meta Data --}}

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="carikotak">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-title" content="carikotak" />

    {{-- Core Meta Data --}}
    <meta name="author" content="carikotak">
    <meta name="description" content="Cari kotak kemasan di kota Anda" />
    <meta name="keywords" content="produk, business, toko, toko online, jasa, distributor, supplier, shop, place" />
    <meta name="revisit-after" content="3 month">
    <meta name="language" content="indonesia, english" />
    <meta http-equiv="Content-Language" content="en-us,id" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    {{-- Robots --}}
    <meta name="robots" content= "index, follow">
    <meta name="googlebot" content="follow">

    {{-- Twitter Cards --}}
    <meta name="twitter:site" content="https://www.carikotak.com">
    <meta name="twitter:title" content="carikotak">
    <meta name="twitter:description" content="Cari kotak kemasan di Kota Anda">
    <meta name="twitter:image:src" content="{{ asset('images/assets/livyl.png') }}">

    {{-- OpenGraph / Facebook --}}
    <meta property="og:title" content="carikotak">
    <meta property="og:site_name" content="carikotak.com">
    <meta property="og:url" content="https://www.carikotak.com/">
    <meta property="og:description" content="Cari kotak kemasan di Kota Anda">
    <meta property="og:type" content="product, business, toko, toko online, distributor, supplier, shop, place">

    {{-- Windows Phone --}}
    <meta name="pjax-timeout" content="1000">
    <meta name="msapplication-TileImage" content="{{ asset('images/assets/livyl.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

		@yield('meta')

    <title>Carikotak.com | Marketplace kotak nomor 1 di Medan.</title>

    @if (env('APP_ENV') === "production")

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/fontawesome/4.6.3/css/font-awesome.min.css">

    @else

        <link rel="stylesheet" href="{{ asset('local/bootstrap-3.3.7/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('local/Font-Awesome-4.7.0/css/font-awesome.min.css') }}" />
        {{-- <link rel="prefetch" as="style" href="{{ asset('local/Open_Sans/open-sans.css') }}" /> --}}

    @endif
    @yield("styles")

</head>

<body>

    @yield('contents')

    @yield("scripts")

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-104467251-1', 'auto');
        ga('send', 'pageview');

    </script>

</body>

</html>
