<?php

use Illuminate\Database\Seeder;
use App\Models\Material;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Material::truncate();

        $materials = [
            [
                'label'         => 'Duplex',
                'description'   => 'Kertas duplex biasanya digunakan sebagai box packaging karena harganya yang relatif murah dibandingkan dengan bahan lainnya. Kertas duplex memiliki satu sisi bewarna putih dan sisi satu lagi berwarna abu-abu.'
            ],
            [
                'label'         => 'Ivory',
                'description'   => 'Kertas ivory memiliki dua sisi berwarna putih, satu sisi berstruktur halus dan satu sisi lagi berstruktur sedikit kesat. Kertas ivory banyak digunakan sebagai kemasan makanan karena jenis kertas yang food grade (tidak berbahaya bagi makanan yang dikonsumsi).'
            ],
            [
                'label'         => 'Art Paper',
                'description'   => 'Kertas art paper digunakan untuk mencetak brosur, majalah, catalog, box atau packaging karena dua bagian mengkilap. Packaging makanan di restoran banyak yang menggunakan kertas art paper sebagai bahan dasar dus makanan, selain aman untuk makanan, kertas ini juga mempunyai kesain lebih eksklusif.'
            ],
            [
                'label'         => 'Laminasi',
                'description'   => 'Kertas laminasi berfungsi sama seperti laminating biasa, hanya dalam bentuk lebih tipis, dan bisa digunakan untuk kemasan produk dus atau lainya.
Kelebihan kertas ini dalam pengemasan sebagai pelindung bahan kertas dari minyak atau air, sehingg minyak atau air tidak mudah tembus pada bagian kertas. Dan juga kertas laminasi dapat mempercantik tampilan kemasan, sehingga lebih terlihat elegan dan sangat ekslusif.'
            ],
            [
                'label'         => 'Kardus',
                'description'   => 'Kertas kardus terbuat dari satu atau beberapa lembar kertas kraft liner dan kertas medium sebagai lapisan gelombangnya.  Adanya lapisan bergelombang yang membedakan dengan bahan kemasain lainnya. Kertas ini juga bisa di padu dengan kertas duplex dan bisa buat kemasan kue dll.'
            ],
            [
                'label'         => 'Karton',
                'description'   => 'Jenis kertas karton cukup tebal, biasa digunakan untuk rangka dalam suatu undangan hard cover, box survernir, box cincin. Bahan ini memberi kesan kokoh dan kuat, tetapi tidak bisa dicetak offset, biasanya dilapis dengan art paper atau duplex dan bahan lainya. Kertas ini dibedakan berdasarkan ketebalannya, biasanya disebut YB 25, 30, 40.'
            ],
            [
                'label'         => 'Lainnya',
                'description'   => 'Material lainnya'
            ]
        ];

        foreach($materials as $m) {
            $material = new Material();
            $material->fill($m);
            $material->save();
        }
    }
}
