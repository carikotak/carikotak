<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::firstOrNew([
            'slug' => 'kotak-kemasan-produk',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Kotak Kemasan Produk',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'kotak-kemasan-hadiah',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Kotak Kemasan Hadiah',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'kreasi-kotak',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Kreasi Kotak',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'kotak-kemasan-daur-ulang',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Kotak Kemasan Daur Ulang',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'uncategorized',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Uncategorized',
            ])->save();
        }
    }
}
