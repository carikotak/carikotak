<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
    	foreach (range(1,10) as $index) {
            Product::create([
                'user_id'       => 1,
                'type'          => 'jual',
                'name'          => $faker->title,
                'price'         => $faker->numberBetween($min = 1000, $max = 9000),
                'category_id'   => 2,
                'description'   => $faker->paragraph,
                'material'      => 1,
                'color'         => $faker->hexColor
            ]);
        }
    }
}
