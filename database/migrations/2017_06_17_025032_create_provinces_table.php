<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('name_abbr', 50);
            $table->string('name_id', 50);
            $table->string('name_en', 50);
            $table->smallInteger('capital_city_id')->unsigned()->index();
            $table->string('iso_code', 5)->index();
            $table->string('iso_name', 50);
            $table->string('iso_type');
            $table->string('iso_geounit', 2)->index();
            $table->tinyInteger('timezone');
            $table->float('lat', 10, 6)->nullable()->index();
            $table->float('lon', 10, 6)->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
