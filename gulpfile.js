'use strict';

const elixir = require('laravel-elixir');
const gutil = require('gulp-util');

require('laravel-elixir-vue-2');
require('laravel-elixir-webpack-official');

const all = gutil.env.all;
const gen = gutil.env.general;
const auth = gutil.env.auth;
const prod = gutil.env.production;

const dev_gnrl = 'general/pages/';
const dev_auth = 'auth/pages/';

const prod_gnrl = 'public/scripts/g/';
const prod_auth = 'public/scripts/a/';

elixir.config.sourcemaps = false;
elixir.config.notifications = false;

elixir(mix => {
  if (gutil.env.home || all || prod) {
    mix.sass('home.scss');
    mix.webpack(dev_gnrl + 'home.js', prod_gnrl + 'home.js');
    mix.webpack(dev_auth + 'home.js', prod_auth + 'home.js');
  }

  if (gutil.env.register || all || prod) {
    mix.sass('register.scss');
    mix.webpack(dev_gnrl + 'register.js', prod_gnrl + 'register.js');
  }

  if (gutil.env.login || all || prod) {
    mix.sass('login.scss');
    mix.webpack(dev_gnrl + 'login.js', prod_gnrl + 'login.js');
  }

  if (gutil.env.create || all || prod) {
    mix.sass('create.scss');
    mix.webpack(dev_auth + 'create.js', prod_auth + 'create.js');
  }

  if (gutil.env.detail || all || prod) {
    mix.sass('detail.scss');
    mix.webpack(dev_gnrl + 'detail.js', prod_gnrl + 'detail.js');
    mix.webpack(dev_auth + 'detail.js', prod_auth + 'detail.js');
  }

  if (gutil.env.product || all || prod) {
    mix.sass('product.scss');
    mix.webpack(dev_gnrl + 'product.js', prod_gnrl + 'product.js');
    mix.webpack(dev_auth + 'product.js', prod_auth + 'product.js');
  }

  if (gutil.env.settings || all || prod) {
    mix.sass('settings.scss');
    mix.webpack(dev_auth + 'settings.js', prod_auth + 'settings.js');
  }

  if (gutil.env.story || all || prod) {
    mix.sass('story.scss');
    mix.webpack(dev_gnrl + 'story.js', prod_gnrl + 'story.js');
    mix.webpack(dev_auth + 'story.js', prod_auth + 'story.js');
  }

  if (gutil.env.search || all || prod) {
    mix.sass('search.scss');
    mix.webpack(dev_gnrl + 'search.js', prod_gnrl + 'search.js');
    mix.webpack(dev_auth + 'search.js', prod_auth + 'search.js');
  }

  if (gutil.env.message || all || prod) {
    mix.sass('message.scss');
    mix.webpack(dev_auth + 'message.js', prod_auth + 'message.js');
  }

  if (gutil.env.article || all || prod) {
    mix.sass('article.scss');
    mix.webpack(dev_auth + 'article.js', prod_auth + 'article.js');
  }

  if (gutil.env.profile || all || prod) {
    mix.sass('profile.scss');
    mix.webpack(dev_gnrl + 'profile.js', prod_gnrl + 'profile.js');
    mix.webpack(dev_auth + 'profile.js', prod_auth + 'profile.js');
  }

  if (gutil.env.wishlist || all || prod) {
    mix.sass('wishlist.scss');
    mix.webpack(dev_auth + 'wishlist.js', prod_auth + 'wishlist.js');
  }

  if (gutil.env.editarticle || all || prod) {
    mix.sass('article.scss');
    mix.webpack(dev_auth + 'edit-article.js', prod_auth + 'edit-article.js');
  }

  if (gutil.env.editproduct || all || prod) {
    mix.sass('create.scss');
    mix.webpack(dev_auth + 'edit-product.js', prod_auth + 'edit-product.js');
  }

  if (gutil.env.notification || all || prod) {
    mix.sass('notification.scss');
    mix.webpack(dev_auth + 'notification.js', prod_auth + 'notification.js');
  }

  if (gutil.env.faq || all || prod) {
    mix.sass('faq.scss');
    mix.webpack(dev_gnrl + 'faq.js', prod_gnrl + 'faq.js');
    mix.webpack(dev_auth + 'faq.js', prod_auth + 'faq.js');
  }

  if (prod) {
    mix.version([
      'css/home.css',
      'scripts/g/home.js',
      'scripts/a/home.js',
      'css/register.css',
      'scripts/g/register.js',
      'css/login.css',
      'scripts/g/login.js',
      'css/create.css',
      'scripts/a/create.js',
      'css/detail.css',
      'scripts/g/detail.js',
      'scripts/a/detail.js',
      'css/product.css',
      'scripts/g/product.js',
      'scripts/a/product.js',
      'css/settings.css',
      'scripts/a/settings.js',
      'css/story.css',
      'scripts/g/story.js',
      'scripts/a/story.js',
      'css/search.css',
      'scripts/g/search.js',
      'scripts/a/search',
      'css/message.css',
      'scripts/a/message.js',
      'css/article.css',
      'scripts/a/article.js',
      'css/profile.css',
      'scripts/g/profile.js',
      'scripts/a/profile.js',
      'css/wishlist.css',
      'scripts/a/wishlist.js',
      'scripts/a/edit-article.js',
      'scripts/a/edit-article.js',
      'scripts/a/edit-product.js',
      'scripts/a/edit-product.js',
      'css/notification.css',
      'scripts/a/notification.js',
      'css/faq.css',
      'scripts/g/faq.js',
      'scripts/a/faq.js'
    ]);
  }

  mix.browserSync({
    files: ['public/css/*.css', 'public/scripts/**/*.js'],
    port: '2000',
    open: false,
    proxy: {
      target: 'http://localhost:8080'
    }
  });
});
