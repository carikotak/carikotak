'use strict'

const elixir = require('laravel-elixir')
const path   = require('path')
const webpack = require('webpack')

const jsPath = elixir.config.get('assets.js.folder')

const env = process.env.NODE_ENV

module.exports = {

    watch: elixir.isWatching(),

    resolve: {

        extensions: ['', '.js', '.vue', '.json'],
        fallback: [
            path.join(__dirname, '../node_modules')
        ],
        alias: {

            generalComponent: path.resolve(__dirname, jsPath, 'general/components'),

            generalPartial: path.resolve(__dirname, jsPath, 'general/partials'),

            authComponent: path.resolve(__dirname, jsPath, 'auth/components'),

            authPartial: path.resolve(__dirname, jsPath, 'auth/partials'),

            lib: path.resolve(__dirname, 'resources/assets/js/lib')
        }
    },

    module: {

        loaders: [
		    {
                test    : /\.js$/,
                include : path.join(__dirname, 'resources/assets'),
                exclude : 'node_modules',
                loader  : 'babel'
            },

            {
                test    : /\.css$/,
                loader  : 'style!css'
            },

            {
                test    : /\.html$/,
                loader  : 'html'
            },

            {
                test    : /\.scss$/,
                loader  : 'style-loader!css-loader!autoprefixer-loader!sass-loader'
            },

            {
                test    : /\.json$/,
                loaders : ["json-loader"]
            }

        ]
    },

    externals: {
        'jquery': 'jQuery'
    }
}
